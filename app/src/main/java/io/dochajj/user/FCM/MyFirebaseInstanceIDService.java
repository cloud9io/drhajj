package io.dochajj.user.FCM;

/**
 * Created by jayakumar on 16/02/17.
 */


import android.util.Log;

import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import io.dochajj.user.Helper.SharedHelper;


public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
    private static final String TAG = MyFirebaseInstanceIDService.class.getSimpleName();
//    Utilities utils = new Utilities();

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        AppEventsLogger.setPushNotificationsRegistrationId(refreshedToken);
        SharedHelper.putKey(getApplicationContext(), "device_token", "" + refreshedToken);
        // HEAD:app/src/main/java/com/keema/userapp/FCM/MyFirebaseInstanceIDService.java
        Log.d(TAG, "onTokenRefresh: ");
        // merged common ancestors
        Log.e(TAG, "" + refreshedToken);
        //
        Log.d(TAG, "onTokenRefresh: ");
        //utils.print(TAG, "onTokenRefresh" + refreshedToken);
        // afe6466c24197202f0b62dc400106d6d220d0cc3:app/src/main/java/com/keema/users/FCM/MyFirebaseInstanceIDService.java
    }
}
