package io.dochajj.user.Helper;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import io.dochajj.user.DocHajj;
import io.dochajj.user.R;

/**
 * Created by ahmed on 10/1/2017.
 */

public class MyGeoCoder {
    private String Address1 = "", Address2 = "", City = "", State = "", Country = "", County = "", PIN = "";
    private String TAG = "myGeoCoder";
    private String strAdd = "";

    public String getAddress(String startLat, String startLng, final Context context) {
        Address1 = "";
        Address2 = "";
        City = "";
        State = "";
        Country = "";
        County = "";
        PIN = "";
        final String url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + startLat + "," + startLng + "&sensor=true&key=" + context.getResources().getString(R.string.google_map_api);
        Log.d(TAG, "getAddress: " + url);
        JSONObject object = new JSONObject();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray results = (JSONArray) response.get("results");
                    JSONObject resultsZero = results.getJSONObject(0);
                    String formatted_address = resultsZero.getString("formatted_address");
                    Log.d(TAG, "onResponse: " + formatted_address);
                    strAdd = formatted_address;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                strAdd = "";
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                return headers;
            }
        };

        DocHajj.getInstance().addToRequestQueue(jsonObjectRequest);
        return strAdd;
    }

    public String getAddress1() {
        return strAdd;
    }

    public String getAddress2() {
        return Address2;

    }

    public String getCity() {
        return City;

    }

    public String getState() {
        return State;

    }

    public String getCountry() {
        return Country;

    }

    public String getCounty() {
        return County;

    }

    public String getPIN() {
        return PIN;

    }

}