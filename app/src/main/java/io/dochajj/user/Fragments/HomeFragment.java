package io.dochajj.user.Fragments;

import android.Manifest;
import android.animation.Animator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.text.format.DateUtils;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.crystal.crystalrangeseekbar.interfaces.OnSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.interfaces.OnSeekbarFinalValueListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalSeekbar;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.kobakei.ratethisapp.RateThisApp;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.skyfishjy.library.RippleBackground;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import io.dochajj.user.Activities.ActivityMobValidation;
import io.dochajj.user.Activities.AddCard;
import io.dochajj.user.Activities.ChatBot;
import io.dochajj.user.Activities.CouponActivity;
import io.dochajj.user.Activities.CustomGooglePlacesSearch;
import io.dochajj.user.Activities.HistoryActivity;
import io.dochajj.user.Activities.ShowProfile;
import io.dochajj.user.DocHajj;
import io.dochajj.user.Helper.AppHelper;
import io.dochajj.user.Helper.ConnectionHelper;
import io.dochajj.user.Helper.CustomDialog;
import io.dochajj.user.Helper.DirectionsJSONParser;
import io.dochajj.user.Helper.SharedHelper;
import io.dochajj.user.Helper.URLHelper;
import io.dochajj.user.Models.CardInfo;
import io.dochajj.user.Models.Driver;
import io.dochajj.user.Models.PlacePredictions;
import io.dochajj.user.R;
import io.dochajj.user.Sinch.LoginActivity;
import io.dochajj.user.Utils.MapAnimator;
import io.dochajj.user.Utils.MapRipple;
import io.dochajj.user.Utils.MyBoldTextView;
import io.dochajj.user.Utils.MyButton;
import io.dochajj.user.Utils.MyTextView;
import io.dochajj.user.Utils.UI.CustomDatePicker;
import io.dochajj.user.Utils.Utilities;

import static io.dochajj.user.DocHajj.trimMessage;
import static io.dochajj.user.FCM.MyFirebaseMessagingService.REQUEST_STATUS_UPDATE;


public class HomeFragment extends Fragment implements OnMapReadyCallback, LocationListener,
        GoogleMap.OnCameraChangeListener, GoogleMap.OnMarkerDragListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, CustomDatePicker.OnCompleteHandler {


    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 123;
    private static final String TAG = "HomeFragment";
    private static final int REQUEST_LOCATION = 1450;
    private static final Interpolator INTERPOLATOR = new FastOutSlowInInterpolator();
    public static final int MOB_VALIDATION_CODE = 1010;
    private final int ADD_CARD_CODE = 435;
    public String PreviousStatus = "";
    public String CurrentStatus = "";
    Activity activity;
    Context context;
    View rootView;
    String isPaid = "", paymentMode = "";
    Utilities utils = new Utilities();
    int flowValue, helpDialog = 0;

    int category;

    TimePickerDialog mTimePicker;
//    CalendarPickerView calendar_view;

    String strCurrentStatus = "";
    DrawerLayout drawer;
    int NAV_DRAWER = 0;
    int PLACE_AUTOCOMPLETE_REQUEST_CODE_DEST = 18945;
    int PROMO_CODE_REQUEST = 55555;
    String feedBackRating = "5";
    double height;
    double width;
    Handler handleCheckStatus;
    String strPickLocation = "", strTag = "", strPickType = "";
    boolean once = true;
    int click = 1;
    int service_cat = 1;
    Number s_hours = 0;
    boolean afterToday = false;
    boolean pick_first = true;
    Driver driver;
    CustomDatePicker cdd;
    //        <!-- Map frame -->
    LinearLayout mapLayout, service_details, lngstep1, lngstep2, lngstep3;
    SupportMapFragment mapFragment;
    GoogleMap mMap;
    int value;
    String sinchNo;
    Marker marker;
    Double latitude, longitude;
    String currentAddress;
    GoogleApiClient mGoogleApiClient;
    //        <!-- Source and Destination Layout-->
    Animation rotation;
    LinearLayout sourceAndDestinationLayout, frmDestination;
    TextView destination, cat_one, cat_two;
    ImageView imgMenu, mapfocus, imgBack, shadowBack;
    View tripLine;
    //       <!--1. Request to providers -->
    ProgressBar getServiceProgress;
    LinearLayout errorLayout;
    LinearLayout lnrRequestProviders, lnrRequestProvidersIN, linSchedule, select_service;
    RecyclerView rcvServiceTypes;
    ImageView imgPaymentType;
    ImageView imgSos;
    ImageView imgShareRide;
    TextView lblPaymentType, lblPaymentChange;
    MyBoldTextView booking_id;
    Button btnRequestRides, btnRequestRidesNow;
    String scheduledDate = "";
    String scheduledTime = "";
    String shownDateAll = "";
    String cancalReason = "";
    LinearLayout lnrEstimatedV;
    TextView surgeAdded;
    TextView lblSurgeEstPrice;
    Button btnCancelEst, btnAcceptEst;

    //        <!--1. Driver Details-->
    String s_gender = "Any";
    LinearLayout lnrHidePopup, lnrSearchAnimation, lnrPriceBase, lnrPricemin, lnrPricekm;
    ImageView imgProviderPopup;

    ImageView imgArrow;
    LinearLayout addPromo;
    TextView theAddedPromo;
    Button btnday1, btnday2, btnday3, btnday4, btntime1, btntime2, btntime3, btntime4;
    ImageButton btndayelse, btntimeelse;
    TextView txtReqInfo;
//    String theTime;


    String haveSurge = "", surgeValue = "";

    //         <!--2. Approximate Rate ...-->
    int startPrice;
    MyBoldTextView lblPriceMin, lblBasePricePopup, lblCapacity, lblServiceName, lblPriceKm, lblCalculationType, lblProviderDesc;
    LinearLayout lnrApproximate;
    Button btnRequestRideConfirm;
    Button imgSchedule;
    CheckBox chkWallet;
    MyBoldTextView lblEta;
    MyBoldTextView lblType;
    TextView lblApproxAmount, service_desc, service_desc2, service_desc3;
    View lineView;
    EditText s_msg;
    LinearLayout ScheduleLayout;
    MyBoldTextView scheduleDate;
    MyBoldTextView scheduleTime;
    MyButton scheduleBtn;
    DatePickerDialog datePickerDialog;

    //         <!--3. Waiting For Providers ...-->
    LocationRequest mLocationRequest;
    RelativeLayout lnrProviderAccepted, lnrWaitingForProviders, lnrProviderPopup;
    //    MyBoldTextView lblNoMatch;
    ImageView imgCenter;
    MyButton btnCancelRide;
    RippleBackground rippleBackground;
    LinearLayout lnrAfterAcceptedStatus, AfterAcceptButtonLayout;
    ImageView imgProvider;

    //         <!--4. Driver Accepted ...-->
    MyBoldTextView lblProvider, lblStatus, lblServiceRequested, lblModelNumber, lblSurgePrice;
    RatingBar ratingProvider;
    MyButton btnCall, btnCancelTrip;
    TextView tvMin, surgeDiscount, surgeTxt;
    RelativeLayout lnrInvoice;
    MyBoldTextView lblBasePrice, lblFeeCharge, lblInvoiceSurgePrice, lblExtraPrice, lblWalletPrice, lblDistancePrice, lblCommision, lblTaxPrice, sbTotal, lblTotalPrice, lblPaymentTypeInvoice,
            lblPaymentChangeInvoice;

    //          <!--5. Invoice Layout ...-->
    ImageView imgPaymentTypeInvoice;
    MyButton btnPayNow;
    LinearLayout lnrRateProvider;
    MyBoldTextView lblProviderNameRate;

    //          <!--6. Rate provider Layout ...-->
    ImageView imgProviderRate;
    RatingBar ratingProviderRate;
    EditText txtCommentsRate;
    Button btnSubmitReview, btnDonePopup;
    Float pro_rating;
    RelativeLayout rtlStaticMarker;
    ImageView imgDestination;

    //            <!-- Static marker-->
    Button btnDone;
    RadioGroup radio;
    RadioButton radioButton;
    CameraPosition cmPosition;
    String current_lat = "", current_lng = "", current_address = "", source_lat = "", source_lng = "", source_address = "",
            dest_lat = "", dest_lng = "", dest_address = "", serv_lat, serv_lng, serv_address;
    //Internet
    ConnectionHelper helper;
    Boolean isInternet;
    //RecylerView
    int currentPostion = -1;
    CustomDialog customDialog;
    //MArkers
    Marker availableProviders;
    ArrayList<LatLng> points = new ArrayList<LatLng>();
    ArrayList<Marker> lstProviderMarkers = new ArrayList<Marker>();
    AlertDialog alert;
    //Animation
    Animation slide_down, slide_up, slide_up_top, slide_up_down;
    ParserTask parserTask;
    String notificationTxt;
    boolean scheduleTrip = false;
    boolean doubleBackToExitPressedOnce = false;
    MapRipple mapRipple;
    //    Verification verification;
    AlertDialog.Builder builder;
    AlertDialog.Builder buildertwo;
    private ArrayList<CardInfo> cardInfoArrayList = new ArrayList<>();
    private boolean mIsShowing;
    private boolean mIsHiding;
    private LatLng sourceLatLng;
    private LatLng destLatLng;
    private Marker sourceMarker;
    private Marker destinationMarker;
    private Marker providerMarker;
    private Fragment mFragment;
    private String[] mSelectedDates;

    LinearLayout catsLayout;
    boolean isChat = false;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        this.mFragment = this;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            notificationTxt = bundle.getString("Notification");
            Log.e("HomeFragment", "onCreate: Notification" + notificationTxt);
        }
        this.mFragment = this;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_home, container, false);

            checkPhoneVerfied();
        }

        customDialog = new CustomDialog(context);
        customDialog.show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                init(rootView);
                //permission to access location
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ActivityCompat.checkSelfPermission(activity,
                        Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // Android M Permission check
                    requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                } else {
                    initMap();
                    MapsInitializer.initialize(getActivity());
                }
            }
        }, 500);


        catsLayout = rootView.findViewById(R.id.lnrCats);

        LinearLayout linCat1 = rootView.findViewById(R.id.cat1);
        linCat1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ChatBot.class));
            }
        });

        LinearLayout linCat2 = rootView.findViewById(R.id.cat2);
        linCat2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
//                isChat = true;
//                sendRequest();
                startActivity(new Intent(getActivity(), LoginActivity.class));
            }
        });

        LinearLayout linCat3 = rootView.findViewById(R.id.cat3);
        linCat3.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                pick_first = true;
                mMap.clear();
                flowValue = 9;
                layoutChanges();
                float zoomLevel = 16.0f; //This goes up to 21
                stopAnim();
            }
        });


        return rootView;
    }

    private void init(final View rootView) {
//        checkAndRequestPermissions();
        helper = new ConnectionHelper(context);
        isInternet = helper.isConnectingToInternet();
        statusCheck();
        getCards();

        handleCheckStatus = new Handler();
        //check status every 3 sec
        handleCheckStatus.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (helper.isConnectingToInternet()) {
                    if (!isAdded()) {
                        return;
                    }
                    utils.print("Handler", "Called");
//                    checkStatus();
                    if (alert != null && alert.isShowing()) {
                        alert.dismiss();
                        alert = null;
                    }
                } else {
                    showDialog();
                }
                handleCheckStatus.postDelayed(this, 3000);
            }
        }, 3000);

//        <!-- Map frame -->
        mapLayout = rootView.findViewById(R.id.mapLayout);
        drawer = activity.findViewById(R.id.drawer_layout);

//        <!-- Source and Destination Layout-->
        sourceAndDestinationLayout = rootView.findViewById(R.id.sourceAndDestinationLayout);
        frmDestination = rootView.findViewById(R.id.frmDestination);
//        source = rootView.findViewById(R.id.source);
        destination = rootView.findViewById(R.id.destination);

        imgMenu = rootView.findViewById(R.id.imgMenu);
        imgSos = rootView.findViewById(R.id.imgSos);
        imgShareRide = rootView.findViewById(R.id.imgShareRide);
        mapfocus = rootView.findViewById(R.id.mapfocus);
        imgBack = rootView.findViewById(R.id.imgBack);
//        shadowBack = rootView.findViewById(R.id.shadowBack);
//        tripLine = rootView.findViewById(R.id.trip_line);

//        <!-- Request to providers-->

        lnrRequestProviders = rootView.findViewById(R.id.lnrRequestProviders);
        lnrRequestProvidersIN = rootView.findViewById(R.id.lnrRequestProvidersIN);
        linSchedule = rootView.findViewById(R.id.linSchedule);
        select_service = rootView.findViewById(R.id.select_service);
        getServiceProgress = rootView.findViewById(R.id.getServiceProgress);
        rcvServiceTypes = rootView.findViewById(R.id.rcvServiceTypes);
//        imgPaymentType = (ImageView) rootView.findViewById(R.id.imgPaymentType);
        lblPaymentType = (MyBoldTextView) rootView.findViewById(R.id.lblPaymentType);
        lblPaymentChange = rootView.findViewById(R.id.lblPaymentChange);
        s_msg = rootView.findViewById(R.id.s_msg);
        booking_id = rootView.findViewById(R.id.booking_id);
        btnRequestRidesNow = rootView.findViewById(R.id.btnRequestRidesNow);
        btnRequestRides = rootView.findViewById(R.id.btnRequestRides);
        lnrEstimatedV = rootView.findViewById(R.id.lnrEstimatedV);
        surgeAdded = rootView.findViewById(R.id.surgeAdded);
        lblSurgeEstPrice = rootView.findViewById(R.id.lblSurgeEstPrice);
        btnCancelEst = rootView.findViewById(R.id.btnCancelEst);
        btnAcceptEst = rootView.findViewById(R.id.btnAcceptEst);

        addPromo = rootView.findViewById(R.id.addPromo);
        theAddedPromo = rootView.findViewById(R.id.theaddedpromo);
        btnday1 = rootView.findViewById(R.id.btnday1);
        btnday2 = rootView.findViewById(R.id.btnday2);
        btnday3 = rootView.findViewById(R.id.btnday3);
        btnday4 = rootView.findViewById(R.id.btnday4);
        btntime1 = rootView.findViewById(R.id.btntime1);
        btntime2 = rootView.findViewById(R.id.btntime2);
        btntime3 = rootView.findViewById(R.id.btntime3);
        btntime4 = rootView.findViewById(R.id.btntime4);
        btndayelse = rootView.findViewById(R.id.btndayelse);
        btntimeelse = rootView.findViewById(R.id.btntimeelse);
        txtReqInfo = rootView.findViewById(R.id.txtReqInfo);

        btnCancelEst.setOnClickListener(new OnClick());
        btnAcceptEst.setOnClickListener(new OnClick());
        addPromo.setOnClickListener(new OnClick());
        btnday1.setOnClickListener(new OnClick());
        btnday2.setOnClickListener(new OnClick());
        btnday3.setOnClickListener(new OnClick());
        btnday4.setOnClickListener(new OnClick());
        btndayelse.setOnClickListener(new OnClick());
        btntime1.setOnClickListener(new OnClick());
        btntime2.setOnClickListener(new OnClick());
        btntime3.setOnClickListener(new OnClick());
        btntime4.setOnClickListener(new OnClick());
        btntimeelse.setOnClickListener(new OnClick());

//        <!--  Driver and service type Details-->

        lnrSearchAnimation = rootView.findViewById(R.id.lnrSearch);
        lnrProviderPopup = rootView.findViewById(R.id.lnrProviderPopup);
        lnrPriceBase = rootView.findViewById(R.id.lnrPriceBase);
//        lnrPricekm = (LinearLayout) rootView.findViewById(R.id.lnrPricekm);
        lnrPricemin = rootView.findViewById(R.id.lnrPricemin);
        lnrHidePopup = rootView.findViewById(R.id.lnrHidePopup);
        service_details = rootView.findViewById(R.id.service_details);

        // request steps
        lngstep1 = rootView.findViewById(R.id.lngstep1);
        lngstep2 = rootView.findViewById(R.id.lngstep2);
        lngstep3 = rootView.findViewById(R.id.lngstep3);

        imgProviderPopup = rootView.findViewById(R.id.imgProviderPopup);
        lblServiceName = rootView.findViewById(R.id.lblServiceName);
//        lblCapacity = (MyBoldTextView) rootView.findViewById(R.id.lblCapacity);
//        lblPriceKm = (MyBoldTextView) rootView.findViewById(R.id.lblPriceKm);
        lblPriceMin = rootView.findViewById(R.id.lblPriceMin);
        lblCalculationType = rootView.findViewById(R.id.lblCalculationType);
        lblBasePricePopup = rootView.findViewById(R.id.lblBasePricePopup);
        lblProviderDesc = rootView.findViewById(R.id.lblProviderDesc);

        btnDonePopup = rootView.findViewById(R.id.btnDonePopup);

//        calendar_view = (CalendarPickerView) rootView.findViewById(R.id.calendar_view);

//         <!--2. Approximate Rate ...-->

        lnrApproximate = rootView.findViewById(R.id.lnrApproximate);
        imgSchedule = rootView.findViewById(R.id.imgSchedule);
        chkWallet = rootView.findViewById(R.id.chkWallet);
//        lblEta = (MyBoldTextView) rootView.findViewById(R.id.lblEta);
//        lblType = (MyBoldTextView) rootView.findViewById(R.id.lblType);
        lblApproxAmount = rootView.findViewById(R.id.lblApproxAmount);
        service_desc = rootView.findViewById(R.id.service_desc);
        service_desc2 = rootView.findViewById(R.id.service_desc2);
        service_desc3 = rootView.findViewById(R.id.service_desc3);
        surgeDiscount = rootView.findViewById(R.id.surgeDiscount);
        surgeTxt = rootView.findViewById(R.id.surge_txt);
        btnRequestRideConfirm = rootView.findViewById(R.id.btnRequestRideConfirm);
        lineView = rootView.findViewById(R.id.lineView);

        //Schedule Layout
        ScheduleLayout = rootView.findViewById(R.id.ScheduleLayout);
        scheduleDate = rootView.findViewById(R.id.scheduleDate);
        scheduleTime = rootView.findViewById(R.id.scheduleTime);
        scheduleBtn = rootView.findViewById(R.id.scheduleBtn);

//         <!--3. Waiting For Providers ...-->

        lnrWaitingForProviders = rootView.findViewById(R.id.lnrWaitingForProviders);
//        lblNoMatch = rootView.findViewById(R.id.lblNoMatch);
        //imgCenter = (ImageView) rootView.findViewById(R.id.imgCenter);
        btnCancelRide = rootView.findViewById(R.id.btnCancelRide);
//        rippleBackground = rootView.findViewById(R.id.content);

//          <!--4. Driver Accepted ...-->

        lnrProviderAccepted = rootView.findViewById(R.id.lnrProviderAccepted);
//        lnrAfterAcceptedStatus = rootView.findViewById(R.id.lnrAfterAcceptedStatus);
        AfterAcceptButtonLayout = rootView.findViewById(R.id.AfterAcceptButtonLayout);
        imgProvider = rootView.findViewById(R.id.imgProvider);
//        imgServiceRequested = rootView.findViewById(R.id.imgServiceRequested);
        lblProvider = rootView.findViewById(R.id.lblProvider);
//        lblStatus = rootView.findViewById(R.id.lblStatus);
        lblSurgePrice = rootView.findViewById(R.id.lblSurgePrice);
//        lblServiceRequested = (MyBoldTextView) rootView.findViewById(R.id.lblServiceRequested);
//        lblModelNumber = (MyBoldTextView) rootView.findViewById(R.id.lblModelNumber);
        ratingProvider = rootView.findViewById(R.id.ratingProvider);
        btnCall = rootView.findViewById(R.id.btnCall);
        btnCancelTrip = rootView.findViewById(R.id.btnCancelTrip);


//           <!--5. Invoice Layout ...-->

        lnrInvoice = rootView.findViewById(R.id.lnrInvoice);
        lblBasePrice = rootView.findViewById(R.id.lblBasePrice);
        lblFeeCharge = rootView.findViewById(R.id.lblFeeCharge);
        lblInvoiceSurgePrice = rootView.findViewById(R.id.lblInvoiceSurgePrice);
        lblExtraPrice = rootView.findViewById(R.id.lblExtraPrice);
        lblWalletPrice = rootView.findViewById(R.id.lblWalletPrice);
//        lblDistancePrice = (MyBoldTextView) rootView.findViewById(R.id.lblDistancePrice);
        //lblCommision = (MyBoldTextView) rootView.findViewById(R.id.lblCommision);
        lblTaxPrice = rootView.findViewById(R.id.lblTaxPrice);
        lblTotalPrice = rootView.findViewById(R.id.lblTotalPrice);
        sbTotal = rootView.findViewById(R.id.sbTotal);
        lblPaymentTypeInvoice = rootView.findViewById(R.id.lblPaymentTypeInvoice);
        imgPaymentTypeInvoice = rootView.findViewById(R.id.imgPaymentTypeInvoice);
        btnPayNow = rootView.findViewById(R.id.btnPayNow);

//          <!--6. Rate provider Layout ...-->

        lnrRateProvider = rootView.findViewById(R.id.lnrRateProvider);
        lblProviderNameRate = rootView.findViewById(R.id.lblProviderName);
        imgProviderRate = rootView.findViewById(R.id.imgProviderRate);
        txtCommentsRate = rootView.findViewById(R.id.txtComments);
        ratingProviderRate = rootView.findViewById(R.id.ratingProviderRate);
        ratingProviderRate.setRating(5);
        ratingProvider.setIsIndicator(true);
        btnSubmitReview = (MyButton) rootView.findViewById(R.id.btnSubmitReview);


//        radio buttons

        radio = rootView.findViewById(R.id.radio);
        radio.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                                             @Override
                                             public void onCheckedChanged(RadioGroup group, int checkedId) {

                                                 radioButton = rootView.findViewById(checkedId);
                                                 s_gender = radioButton.getText().toString();
                                                 Log.d(TAG, "onCheckedChanged: " + s_gender);
                                             }
                                         }
        );

//            <!--Static marker-->

        rtlStaticMarker = rootView.findViewById(R.id.rtlStaticMarker);
        imgDestination = rootView.findViewById(R.id.imgDestination);
        btnDone = rootView.findViewById(R.id.btnDone);

        rotation = AnimationUtils.loadAnimation(context, R.anim.rotation);
        rotation.setRepeatCount(Animation.INFINITE);

        btnRequestRidesNow.setOnClickListener(new OnClick());
        btnRequestRides.setOnClickListener(new OnClick());
        btnDonePopup.setOnClickListener(new OnClick());
        lnrHidePopup.setOnClickListener(new OnClick());
        btnRequestRideConfirm.setOnClickListener(new OnClick());
        btnCancelRide.setOnClickListener(new OnClick());
        btnCancelTrip.setOnClickListener(new OnClick());
        btnCall.setOnClickListener(new OnClick());
        btnPayNow.setOnClickListener(new OnClick());
        btnSubmitReview.setOnClickListener(new OnClick());
        btnDone.setOnClickListener(new OnClick());
        frmDestination.setOnClickListener(new OnClick());
        lblPaymentChange.setOnClickListener(new OnClick());
        imgMenu.setOnClickListener(new OnClick());
        mapfocus.setOnClickListener(new OnClick());
        imgSchedule.setOnClickListener(new OnClick());
        imgBack.setOnClickListener(new OnClick());
        scheduleBtn.setOnClickListener(new OnClick());
        scheduleDate.setOnClickListener(new OnClick());
        scheduleTime.setOnClickListener(new OnClick());
        imgProvider.setOnClickListener(new OnClick());
        imgProviderRate.setOnClickListener(new OnClick());
        imgSos.setOnClickListener(new OnClick());
        imgShareRide.setOnClickListener(new OnClick());
        select_service.setOnClickListener(new OnClick());
//        cat_one.setOnClickListener(new OnClick());
//        cat_two.setOnClickListener(new OnClick());
//        seekbar

        final CrystalSeekbar CrystalSeekbar = rootView.findViewById(R.id.rangeSeekbar2);

// get min and max text view
        tvMin = rootView.findViewById(R.id.textMin5);

// set listener
        CrystalSeekbar.setOnSeekbarChangeListener(new OnSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue) {
                s_hours = minValue;
                tvMin.setText(String.valueOf(minValue));
            }
        });

// set final value listener
        CrystalSeekbar.setOnSeekbarFinalValueListener(new OnSeekbarFinalValueListener() {
            @Override
            public void finalValue(Number value) {
                Log.d("CRS=>", String.valueOf(value));
            }
        });


        flowValue = 0;
        layoutChanges();
        getServiceList();
        Log.e("Handler", "layoutChanges Called in init");

        //Load animation
        slide_down = AnimationUtils.loadAnimation(context, R.anim.slide_down);
        slide_up = AnimationUtils.loadAnimation(context, R.anim.slide_up);
        slide_up_top = AnimationUtils.loadAnimation(context, R.anim.slide_up_top);
        slide_up_down = AnimationUtils.loadAnimation(context, R.anim.slide_up_down);


        rootView.setFocusableInTouchMode(true);
        rootView.requestFocus();
        rootView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() != KeyEvent.ACTION_DOWN)
                    return true;

                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    utils.print("Handle EVENT", "Back key pressed!");


                    if (linSchedule.getVisibility() == View.VISIBLE) {
                        linSchedule.setVisibility(View.GONE);
                        mSelectedDates = null;
                        scheduledTime = "";
                        btnRequestRides.setVisibility(View.VISIBLE);
                    } else if (lnrEstimatedV.getVisibility() == View.VISIBLE) {
                        lnrEstimatedV.setAnimation(slide_down);
                        lnrEstimatedV.setVisibility(View.GONE);
                        lnrRequestProviders.setAnimation(slide_up);
                        lnrRequestProviders.setVisibility(View.VISIBLE);
                    } else if (lnrRequestProvidersIN.getVisibility() == View.VISIBLE) {
                        flowValue = 0;
                        layoutChanges();
                        if (!current_lat.equalsIgnoreCase("") && !current_lng.equalsIgnoreCase("")) {
                            LatLng myLocation = new LatLng(Double.parseDouble(current_lat), Double.parseDouble(current_lng));
                            CameraPosition cameraPosition = new CameraPosition.Builder().target(myLocation).zoom(14).build();
                            mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                        }
                    } else if (lnrProviderPopup.getVisibility() == View.VISIBLE) {
                        flowValue = 1;
                        layoutChanges();
                    } else if (catsLayout.getVisibility() != View.VISIBLE
                            && lnrProviderAccepted.getVisibility() != View.VISIBLE) {
                        catsLayout.setAnimation(slide_up);
                        catsLayout.setVisibility(View.VISIBLE);
                    } else {
                        if (lnrWaitingForProviders.getVisibility() == View.VISIBLE
                                || lnrProviderAccepted.getVisibility() == View.VISIBLE
                                || catsLayout.getVisibility() == View.VISIBLE) {
                            if (doubleBackToExitPressedOnce) {
                                getActivity().finish();
                                return false;
                            }

                            doubleBackToExitPressedOnce = true;
                            Toast.makeText(getActivity(), "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

                            new Handler().postDelayed(new Runnable() {

                                @Override
                                public void run() {
                                    doubleBackToExitPressedOnce = false;
                                }
                            }, 3000);
                            return true;

                        }
                    }

                    return true;
                }
                return false;
            }
        });

    }

    @SuppressWarnings("MissingPermission")
    void initMap() {

        if (mMap == null) {
            FragmentManager fm = getChildFragmentManager();
            mapFragment = ((SupportMapFragment) fm.findFragmentById(R.id.provider_map));
            mapFragment.getMapAsync(this);
        }

        if (mMap != null) {
            setupMap();
        }

    }

    @SuppressWarnings("MissingPermission")
    void setupMap() {
        if (mMap != null) {
//            mMap.setMyLocationEnabled(true);
//            mMap.getUiSettings().setMyLocationButtonEnabled(true);
            mMap.getUiSettings().setCompassEnabled(false);
            mMap.setBuildingsEnabled(true);
            mMap.setMyLocationEnabled(false);
            mMap.setOnMarkerDragListener(this);
            mMap.setOnCameraChangeListener(this);
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(30.0594699, 31.328505), 8));
        }

        mMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
            @Override
            public void onMyLocationChange(Location location) {
                if (value == 0) {
                    LatLng myLocation = new LatLng(location.getLatitude(), location.getLongitude());

//                    CameraPosition cameraPosition = new CameraPosition.Builder().target(myLocation).zoom(14).build();
//                    mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                    mMap.setPadding(0, 0, 0, 0);
                    mMap.getUiSettings().setZoomControlsEnabled(false);
                    mMap.getUiSettings().setMyLocationButtonEnabled(true);
                    mMap.getUiSettings().setMapToolbarEnabled(false);
                    mMap.getUiSettings().setCompassEnabled(false);

                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                    currentAddress = utils.getCompleteAddressString(context, latitude, longitude);
                    source_lat = "" + latitude;
                    source_lng = "" + longitude;
                    source_address = currentAddress;
                    current_address = currentAddress;
                    value++;
                }

                current_lat = "" + location.getLatitude();
                current_lng = "" + location.getLongitude();
            }
        });

    }

    @Override
    public void onLocationChanged(Location location) {

        if (marker != null) {
            marker.remove();
        }

        MarkerOptions markerOptions = new MarkerOptions()
                .position(new LatLng(location.getLatitude(), location.getLongitude()))
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.current_location));
        marker = mMap.addMarker(markerOptions);

        // mMap is GoogleMap object, latLng is the location on map from which ripple should start

       /* if (mapRipple == null){
            mapRipple = new MapRipple(mMap, new LatLng(location.getLatitude(),location.getLongitude()), context);
            mapRipple.startRippleMapAnimation();
        }else{
            mapRipple.withLatLng(new LatLng(location.getLatitude(), location.getLongitude()));
        }

        // Start Animation again only if it is not running
        if (!mapRipple.isAnimationRunning()) {
            mapRipple.startRippleMapAnimation();
        }*/
        Log.e("MAP", "onLocationChanged: 1 " + location.getLatitude());
        Log.e("MAP", "onLocationChanged: 2 " + location.getLongitude());
        current_lat = "" + location.getLatitude();
        current_lng = "" + location.getLongitude();
//        dest_lat = current_lat;
//        dest_lng = current_lng;
        currentAddress = " ";

        if (value == 0) {
            LatLng myLocation = new LatLng(location.getLatitude(), location.getLongitude());
            CameraPosition cameraPosition = new CameraPosition.Builder().target(myLocation).zoom(16).build();
            mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            mMap.setPadding(0, 0, 0, 0);
            mMap.getUiSettings().setZoomControlsEnabled(false);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
            mMap.getUiSettings().setMapToolbarEnabled(false);
            mMap.getUiSettings().setCompassEnabled(false);


            latitude = location.getLatitude();
            longitude = location.getLongitude();

            final String url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + Double.toString(latitude) + "," + Double.toString(longitude) + "&sensor=true&key=" + context.getResources().getString(R.string.google_map_api);
            Log.d(TAG, "getAddress: " + url);
            JSONObject object = new JSONObject();
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, object, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        JSONArray results = (JSONArray) response.get("results");
                        JSONObject resultsZero = results.getJSONObject(0);
                        String formatted_address = resultsZero.getString("formatted_address");
                        Log.d(TAG, "onResponse: " + formatted_address);
                        currentAddress = formatted_address;
                        current_address = currentAddress;
                        Log.d(TAG, "onLocationChanged: " + currentAddress);
                        source_lat = "" + current_lat;
                        source_lng = "" + current_lng;
                        source_address = currentAddress;
//            dest_address = current_address;
//            dest_lat = source_lat;
//            dest_lng = source_lng;
                        current_address = currentAddress;

                        serv_lat = current_lat;
                        serv_lng = current_lng;
                        serv_address = current_address;
                        dest_lat = "" + current_lat;
                        dest_lng = "" + current_lng;
//                        select_service.setVisibility(View.GONE);

//            getProvidersList("");
//
//            flowValue = 1;
//            layoutChanges();


                        value++;
                        customDialog.dismiss();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    currentAddress = "";
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    return headers;
                }
            };

            DocHajj.getInstance().addToRequestQueue(jsonObjectRequest);
        }
    }

    public void navigateToShareScreen(String shareUrl) {
        try {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            String name = SharedHelper.getKey(context, "first_name") + " " + SharedHelper.getKey(context, "last_name");
            sendIntent.putExtra(Intent.EXTRA_TEXT, "7keema-" + "Mr/Mrs." + name + " would like to share 7keema with you at " +
                    shareUrl + current_lat + "," + current_lng);
            sendIntent.setType("text/plain");
            startActivity(sendIntent);
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, "Share applications not found!", Toast.LENGTH_SHORT).show();
        }

    }

    private void showSosPopUp() {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        builder.setTitle(context.getString(R.string.app_name))
                .setIcon(R.mipmap.ic_launcher)
                .setMessage(getString(R.string.emaergeny_call))
                .setCancelable(false);
        builder.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, 3);
                } else {
                    Intent intentCall = new Intent(Intent.ACTION_CALL);
                    intentCall.setData(Uri.parse("tel:" + SharedHelper.getKey(context, "sos")));
                    startActivity(intentCall);
                }
            }
        });
        builder.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void sosRequest() {

    }

    private void showCancelRideDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        builder.setTitle(context.getString(R.string.app_name))
                .setIcon(R.mipmap.ic_launcher)
                .setMessage(getString(R.string.cancel_ride_alert));
        builder.setCancelable(false);
        builder.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                showreasonDialog();
            }
        });
        builder.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void showreasonDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = LayoutInflater.from(context).inflate(R.layout.cancel_dialog, null);
        final EditText reasonEtxt = view.findViewById(R.id.reason_etxt);
        Button submitBtn = view.findViewById(R.id.submit_btn);
        builder.setIcon(R.mipmap.ic_launcher)
                .setTitle(R.string.app_name)
                .setView(view)
                .setCancelable(true);
        final AlertDialog dialog = builder.create();
        submitBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                cancalReason = reasonEtxt.getText().toString();
                cancelRequest();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    void layoutChanges() {
        Log.v("Handler", "layoutChanges Called");
        try {
            checkStatus();
            utils.hideKeypad(getActivity(), getActivity().getCurrentFocus());
            if (lnrApproximate.getVisibility() == View.VISIBLE) {
                lnrApproximate.startAnimation(slide_down);
            } else if (ScheduleLayout.getVisibility() == View.VISIBLE) {
                ScheduleLayout.startAnimation(slide_down);
//            } else if (lnrRequestProviders.getVisibility() == View.VISIBLE) {
//                lnrRequestProviders.startAnimation(slide_down);
            } else if (lnrProviderPopup.getVisibility() == View.VISIBLE) {
                lnrProviderPopup.startAnimation(slide_down);
                lnrSearchAnimation.startAnimation(slide_up_down);
                lnrSearchAnimation.setVisibility(View.VISIBLE);
                lnrRequestProviders.setAnimation(slide_up);
                lnrRequestProviders.setVisibility(View.VISIBLE);
            } else if (lnrInvoice.getVisibility() == View.VISIBLE) {
                lnrInvoice.startAnimation(slide_down);
            } else if (lnrRateProvider.getVisibility() == View.VISIBLE) {
                lnrRateProvider.startAnimation(slide_down);
            }
            lnrRequestProvidersIN.setVisibility(View.GONE);
            lnrProviderPopup.setVisibility(View.GONE);
            lnrApproximate.setVisibility(View.GONE);
            lnrWaitingForProviders.setVisibility(View.GONE);
            lnrProviderAccepted.setVisibility(View.GONE);
            lnrInvoice.setVisibility(View.GONE);
            lnrRateProvider.setVisibility(View.GONE);
            ScheduleLayout.setVisibility(View.GONE);
            rtlStaticMarker.setVisibility(View.GONE);
            imgBack.setVisibility(View.GONE);
            txtCommentsRate.setText("");
            scheduleDate.setText("" + context.getString(R.string.sample_date));
            scheduleTime.setText("" + context.getString(R.string.sample_time));


            btnday1.setBackgroundResource(R.drawable.buttonshape);
            btnday1.setTextColor(getResources().getColor(R.color.black_text_color));
            btnday2.setBackgroundResource(R.drawable.buttonshape);
            btnday2.setTextColor(getResources().getColor(R.color.black_text_color));
            btnday3.setBackgroundResource(R.drawable.buttonshape);
            btnday3.setTextColor(getResources().getColor(R.color.black_text_color));
            btnday4.setBackgroundResource(R.drawable.buttonshape);
            btnday4.setTextColor(getResources().getColor(R.color.black_text_color));
            btntime1.setBackgroundResource(R.drawable.buttonshape);
            btntime1.setTextColor(getResources().getColor(R.color.black_text_color));
            btntime2.setBackgroundResource(R.drawable.buttonshape);
            btntime2.setTextColor(getResources().getColor(R.color.black_text_color));
            btntime3.setBackgroundResource(R.drawable.buttonshape);
            btntime3.setTextColor(getResources().getColor(R.color.black_text_color));
            btntime4.setBackgroundResource(R.drawable.buttonshape);
            btntime4.setTextColor(getResources().getColor(R.color.black_text_color));
            scheduledTime = "";
            mSelectedDates = null;
            s_msg.setText("");


            if (flowValue == 0) {

                lnrSearchAnimation.setVisibility(View.VISIBLE);
                if (imgMenu.getVisibility() == View.GONE) {
                    if (mMap != null) {
                        mMap.clear();
                        stopAnim();
                        setupMap();
                    }
                }
                AppHelper.setDest(destination, current_address);
                dest_address = "";
                dest_lat = "";
                dest_lng = "";
                source_lat = "" + current_lat;
                source_lng = "" + current_lng;
                source_address = "" + current_address;
                sourceAndDestinationLayout.setVisibility(View.VISIBLE);
                catsLayout.setVisibility(View.VISIBLE);

//                txtReqInfo.setText(R.string.please_select_service);
//                txtReqInfo.setVisibility(View.VISIBLE);
//                lnrRequestProviders.setVisibility(View.VISIBLE);
//                lnrRequestProvidersIN.setVisibility(View.GONE);
//                lnrProviderPopup.setVisibility(View.GONE);

            } else if (flowValue == 1) {

                lnrSearchAnimation.setVisibility(View.VISIBLE);
                lnrSearchAnimation.setAnimation(slide_up_down);
//                lnrRequestProviders.setAnimation(slide_up);
//                lnrRequestProviders.setVisibility(View.VISIBLE);
//
//                txtReqInfo.setText(R.string.please_select_service);
//                txtReqInfo.setVisibility(View.VISIBLE);
//                lnrRequestProvidersIN.setVisibility(View.GONE);
//                lnrProviderPopup.setVisibility(View.GONE);

                if (destinationMarker != null) {
                    destinationMarker.setDraggable(true);
                    destinationMarker.setDraggable(true);
                }

            } else if (flowValue == 2) {

//                lnrRequestProviders.setVisibility(View.GONE);
                mapfocus.setVisibility(View.GONE);
                imgMenu.setVisibility(View.GONE);

                imgBack.setVisibility(View.VISIBLE);
                chkWallet.setChecked(false);
//                lnrApproximate.startAnimation(slide_up);
//                lnrApproximate.setVisibility(View.VISIBLE);
                if (destinationMarker != null) {
                    destinationMarker.setDraggable(false);
                    destinationMarker.setDraggable(false);
                }
            } else if (flowValue == 3) {

                lnrRequestProviders.setVisibility(View.GONE);
                lnrEstimatedV.setVisibility(View.GONE);
                lnrSearchAnimation.setVisibility(View.GONE);
                if (isChat) {

                } else {
                    lnrWaitingForProviders.setVisibility(View.VISIBLE);
                }
                //sourceAndDestinationLayout.setVisibility(View.GONE);
            } else if (flowValue == 4) {

                lnrRequestProviders.setVisibility(View.GONE);
                lnrSearchAnimation.setVisibility(View.VISIBLE);
                lnrProviderAccepted.startAnimation(slide_up);
                lnrProviderAccepted.setVisibility(View.VISIBLE);

            } else if (flowValue == 5) {

                lnrRequestProviders.setVisibility(View.GONE);
//                lnrSearchAnimation.setVisibility(View.VISIBLE);
//                lnrInvoice.startAnimation(slide_up);
//                lnrInvoice.setVisibility(View.VISIBLE);

            } else if (flowValue == 6) {

                lnrRequestProviders.setVisibility(View.GONE);
                lnrSearchAnimation.setVisibility(View.VISIBLE);
                lnrRateProvider.startAnimation(slide_up);
                lnrRateProvider.setVisibility(View.VISIBLE);
                LayerDrawable drawable = (LayerDrawable) ratingProviderRate.getProgressDrawable();
                drawable.getDrawable(0).setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);
                drawable.getDrawable(1).setColorFilter(Color.parseColor("#FFAB00"), PorterDuff.Mode.SRC_ATOP);
                drawable.getDrawable(2).setColorFilter(Color.parseColor("#FFAB00"), PorterDuff.Mode.SRC_ATOP);
                ratingProviderRate.setRating(5.0f);
                feedBackRating = "5";
                ratingProviderRate.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                    @Override
                    public void onRatingChanged(RatingBar ratingBar, float rating, boolean b) {
                        if (rating < 1.0f) {
                            ratingProviderRate.setRating(5.0f);
                            feedBackRating = "1";
                        }
                        feedBackRating = String.valueOf((int) rating);
                    }
                });

            } else if (flowValue == 7) {

                lnrRequestProviders.setVisibility(View.GONE);
                imgMenu.setVisibility(View.GONE);
                imgBack.setVisibility(View.VISIBLE);
                ScheduleLayout.startAnimation(slide_up);
                ScheduleLayout.setVisibility(View.VISIBLE);

            } else if (flowValue == 8) {

                mapfocus.setVisibility(View.GONE);
                lnrRequestProviders.setVisibility(View.GONE);

            } else if (flowValue == 9) {
                catsLayout.setVisibility(View.GONE);
                mapfocus.setVisibility(View.VISIBLE);
                imgMenu.setVisibility(View.VISIBLE);
                lnrRequestProviders.setVisibility(View.GONE);
                rtlStaticMarker.setVisibility(View.VISIBLE);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        try {
            boolean success = googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            getActivity(), R.raw.style_json));

            if (!success) {
                utils.print("Map:Style", "Style parsing failed.");
            } else {
                utils.print("Map:Style", "Style Applied.");
            }
        } catch (Resources.NotFoundException e) {
            utils.print("Map:Style", "Can't find style. Error: ");
        }

        mMap = googleMap;

        setupMap();

        //Initialize Google Play Services
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                //Location Permission already granted
                buildGoogleApiClient();
//                mMap.setMyLocationEnabled(true);
            } else {
                //Request Location Permission
                checkLocationPermission();
            }
        } else {
            buildGoogleApiClient();
//            mMap.setMyLocationEnabled(true);
        }
    }

    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(context)
                        .setTitle("Location Permission Needed")
                        .setMessage("This app needs the Location permission, please accept to use location functionality")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(getActivity(),
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        1);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        1);
            }
        }

    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onCameraChange(CameraPosition cameraPosition) {
        if (lstProviderMarkers.size() > 0) {
            lstProviderMarkers.get(0).setVisible(cameraPosition.zoom > 7);
        }
        if (strPickLocation.equalsIgnoreCase("yes")) {
            cmPosition = cameraPosition;
            if (pick_first) {
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(cmPosition.target.latitude,
                        cmPosition.target.longitude), 16.0f));
                pick_first = false;
            }
        }
    }

//    @TargetApi(Build.VERSION_CODES.M)
//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        switch (requestCode) {
//            case 1:
//                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    // Permission Granted
//                    //Toast.makeText(SignInActivity.this, "PERMISSION_GRANTED", Toast.LENGTH_SHORT).show();
//                    initMap();
//                    MapsInitializer.initialize(getActivity());
//                } /*else {
//                    showPermissionReqDialog();
//                }*/
//                break;
//            case 2:
//                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    // Permission Granted
//                    //Toast.makeText(SignInActivity.this, "PERMISSION_GRANTED", Toast.LENGTH_SHORT).show();
//                    Intent intent = new Intent(Intent.ACTION_CALL);
//                    intent.setData(Uri.parse("tel:" + SharedHelper.getKey(context, "provider_mobile_no")));
//                    startActivity(intent);
//                } else {
//                    requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, 1);
//                }
//                break;
//            case 3:
//                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    // Permission Granted
//                    //Toast.makeText(SignInActivity.this, "PERMISSION_GRANTED", Toast.LENGTH_SHORT).show();
//                    Intent intent = new Intent(Intent.ACTION_CALL);
//                    intent.setData(Uri.parse("tel:" + SharedHelper.getKey(context, "sos")));
//                    startActivity(intent);
//                } else {
//                    requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, 1);
//                }
//                break;
//            default:
//                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        }
//    }

    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {

    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        String title = "";
        if (marker.getTitle() != null) {
            title = marker.getTitle();
            if (title.equalsIgnoreCase("Source")) {
                LatLng markerLocation = destinationMarker.getPosition();
                Geocoder geocoder;
                List<Address> addresses = null;
                geocoder = new Geocoder(getActivity(), Locale.getDefault());

                source_lat = markerLocation.latitude + "";
                source_lng = markerLocation.longitude + "";

                try {
                    addresses = geocoder.getFromLocation(markerLocation.latitude, markerLocation.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                    if (addresses.size() > 0) {
                        String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                        String city = addresses.get(0).getLocality();
                        String state = addresses.get(0).getAdminArea();
                        // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                        SharedHelper.putKey(context, "source", "" + address + "," + city + "," + state);
                        source_address = "" + address + "," + city + "," + state;
                    } else {
                        final String surl = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + Double.toString(latitude) + "," + Double.toString(longitude) + "&sensor=true&key=" + context.getResources().getString(R.string.google_map_api);
                        Log.d(TAG, "getAddress: " + surl);
                        JSONObject object = new JSONObject();
                        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, surl, object, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    JSONArray results = (JSONArray) response.get("results");
                                    JSONObject resultsZero = results.getJSONObject(0);
                                    String formatted_address = resultsZero.getString("formatted_address");
                                    Log.d(TAG, "onResponse: " + formatted_address);
                                    source_address = formatted_address;
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                source_address = "";
                            }
                        }) {
                            @Override
                            public Map<String, String> getHeaders() throws AuthFailureError {
                                HashMap<String, String> headers = new HashMap<String, String>();
                                return headers;
                            }
                        };

                        DocHajj.getInstance().addToRequestQueue(jsonObjectRequest);
                    }
                } catch (Exception e) {
                    final String surl = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + Double.toString(latitude) + "," + Double.toString(longitude) + "&sensor=true&key=" + context.getResources().getString(R.string.google_map_api);
                    Log.d(TAG, "getAddress: " + surl);
                    JSONObject object = new JSONObject();
                    JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, surl, object, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONArray results = (JSONArray) response.get("results");
                                JSONObject resultsZero = results.getJSONObject(0);
                                String formatted_address = resultsZero.getString("formatted_address");
                                Log.d(TAG, "onResponse: " + formatted_address);
                                source_address = formatted_address;
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            source_address = "";
                        }
                    }) {
                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            HashMap<String, String> headers = new HashMap<String, String>();
                            return headers;
                        }
                    };

                    DocHajj.getInstance().addToRequestQueue(jsonObjectRequest);
                    e.printStackTrace();
                }

            } else if (destinationMarker != null && title.equalsIgnoreCase("Destination")) {
//                LatLng markerLocation = destinationMarker.getPosition();
                Geocoder geocoder;
                List<Address> addresses;
                geocoder = new Geocoder(getActivity(), Locale.getDefault());

//                dest_lat = "" + markerLocation.latitude;
//                dest_lng = "" + markerLocation.longitude;

//                try {
////                    addresses = geocoder.getFromLocation(markerLocation.latitude, markerLocation.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
//                    if (addresses.size() > 0) {
//                        String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
//                        String city = addresses.get(0).getLocality();
//                        String state = addresses.get(0).getAdminArea();
//                        ; // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
//                        SharedHelper.putKey(context, "destination", "" + address + "," + city + "," + state);
//                        dest_address = "" + address + "," + city + "," + state;
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }

            }
            mMap.clear();
//            setValuesForSourceAndDestination();
        }
    }

    private void showPermissionReqDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        builder.setTitle(context.getString(R.string.app_name))
                .setIcon(R.mipmap.ic_launcher)
                .setCancelable(false)
                .setMessage("7keema needs the location permission, Please accept to use location functionality")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //Prompt the user once explanation has been shown
                        // requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                        Intent intent = new Intent();
                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", activity.getPackageName(), HomeFragment.TAG);
                        intent.setData(uri);
                        startActivity(intent);
                    }
                })
                .create()
                .show();
    }

    private void showDialogForGPSIntent() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        builder.setTitle(context.getString(R.string.app_name))
                .setIcon(R.mipmap.ic_launcher)
                .setMessage("GPS is disabled in your device. Enable it?")
                .setCancelable(false)
                .setPositiveButton("Enable GPS",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                Intent callGPSSettingIntent = new Intent(
                                        Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                activity.startActivity(callGPSSettingIntent);
                            }
                        });
        builder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert1 = builder.create();
        alert1.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PROMO_CODE_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                theAddedPromo.setText(data.getStringExtra("addedPromo"));
                theAddedPromo.setCompoundDrawablesWithIntrinsicBounds(R.drawable.discountok, 0, 0, 0);
            }
        }

        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE_DEST) {


            if (resultCode == Activity.RESULT_OK) {
                if (marker != null) {
                    marker.remove();
                }
                PlacePredictions placePredictions;
                placePredictions = (PlacePredictions) data.getSerializableExtra("Location Address");
                strPickLocation = data.getExtras().getString("pick_location");
                strPickType = data.getExtras().getString("type");
                if (strPickLocation.equalsIgnoreCase("yes")) {
                    pick_first = true;
                    mMap.clear();
                    flowValue = 9;
                    layoutChanges();
                    float zoomLevel = 16.0f; //This goes up to 21
                    stopAnim();
                } else {
                    if (placePredictions != null) {

                        source_address = placePredictions.strDestAddress;
                        source_lat = placePredictions.strDestLatitude;
                        source_lng = placePredictions.strDestLongitude;

                        double latitude = Double.parseDouble(placePredictions.strDestLatitude);
                        double longitude = Double.parseDouble(placePredictions.strDestLongitude);

                        LatLng location = new LatLng(latitude, longitude);

                        mMap.clear();
                        MarkerOptions markerOptions = new MarkerOptions()
                                .position(location)
                                .title("Service Location")
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.mypos));

                        marker = mMap.addMarker(markerOptions);
                        CameraPosition cameraPosition = new CameraPosition.Builder().target(location).zoom(16).build();
                        mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                        AppHelper.setDest(destination, "" + placePredictions.strDestAddress);

                        if (dest_address.equalsIgnoreCase("")) {
                            flowValue = 1;
                            AppHelper.setDest(destination, "" + source_address);
                            getServiceList();
                        } else {
                            flowValue = 1;
                            if (cardInfoArrayList.size() > 0) {
                                getCardDetailsForPayment(cardInfoArrayList.get(0));
                            }
                            getServiceList();
                        }
                        layoutChanges();
                    }
                }
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(getActivity(), data);
                // TODO: Handle the error.
            } else if (resultCode == Activity.RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
        if (requestCode == ADD_CARD_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                boolean result = data.getBooleanExtra("isAdded", false);
                if (result) {
                    getCards();
                }
            }
        }
        if (requestCode == REQUEST_LOCATION) {
            Log.e("GPS Result Status", "onActivityResult: " + requestCode);
            Log.e("GPS Result Status", "onActivityResult: " + data);
        }

    }

    void showProviderPopup(JSONObject jsonObject) {
        // hide ui
        lnrSearchAnimation.startAnimation(slide_up_top);
        lnrSearchAnimation.setVisibility(View.GONE);

        lnrRequestProviders.setAnimation(slide_down);
        lnrRequestProviders.setVisibility(View.GONE);

        // animate details
        lnrProviderPopup.setVisibility(View.VISIBLE);
        lnrHidePopup.setVisibility(View.VISIBLE);

        service_details.startAnimation(slide_up);
        service_details.setVisibility(View.VISIBLE);


        Glide.with(activity).load(jsonObject.optString("image"))
                .placeholder(R.drawable.doctor_male)
                .dontAnimate()
                .error(R.drawable.doctor_male)
                .into(imgProviderPopup);

        lnrPriceBase.setVisibility(View.GONE);
        lnrPricemin.setVisibility(View.GONE);
//        lnrPricekm.setVisibility(View.GONE);

        if (jsonObject.optString("calculator").equalsIgnoreCase("MIN")
                || jsonObject.optString("calculator").equalsIgnoreCase("HOUR")) {
            lnrPriceBase.setVisibility(View.VISIBLE);
            lnrPricemin.setVisibility(View.VISIBLE);
            if (jsonObject.optString("calculator").equalsIgnoreCase("MIN")) {
                lblCalculationType.setText("Minutes");
            } else {
                lblCalculationType.setText("Hours");
            }
        } else if (jsonObject.optString("calculator").equalsIgnoreCase("DISTANCE")) {
            lnrPriceBase.setVisibility(View.VISIBLE);
//            lnrPricekm.setVisibility(View.VISIBLE);
            lblCalculationType.setText("Distance");
        } else if (jsonObject.optString("calculator").equalsIgnoreCase("DISTANCEMIN")
                || jsonObject.optString("calculator").equalsIgnoreCase("DISTANCEHOUR")) {
            lnrPriceBase.setVisibility(View.VISIBLE);
            lnrPricemin.setVisibility(View.VISIBLE);
//            lnrPricekm.setVisibility(View.VISIBLE);
            if (jsonObject.optString("calculator").equalsIgnoreCase("DISTANCEMIN")) {
                lblCalculationType.setText("Distance and Minutes");
            } else {
                lblCalculationType.setText("Distance and Hours");
            }
        }

        if (!jsonObject.optString("capacity").equalsIgnoreCase("null")) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                lblCapacity.setText(jsonObject.optString("capacity"));
            } else {
//                lblCapacity.setText(jsonObject.optString("capacity") +" peoples");
            }
        } else {
//            lblCapacity.setVisibility(View.GONE);
        }
        lblServiceName.setText("" + jsonObject.optString("name"));
//        lblBasePricePopup.setText(SharedHelper.getKey(context, "currency") + jsonObject.optString("fixed"));
//        lblPriceKm.setText(SharedHelper.getKey(context, "currency") + jsonObject.optString("price"));
//        lblPriceMin.setText(SharedHelper.getKey(context, "currency") + jsonObject.optString("minute"));

        int basePrice = Integer.parseInt(jsonObject.optString("fixed"));
        int hourPrice = Integer.parseInt(jsonObject.optString("minute"));
        int distPrice = Integer.parseInt(jsonObject.optString("distance")) * 10;
        startPrice = basePrice + hourPrice;

        String estFare = SharedHelper.getKey(context, "currency") + " " + startPrice + "-" + String.valueOf(Integer.valueOf(startPrice + hourPrice + distPrice));
        lblBasePricePopup.setText(estFare);
        if (jsonObject.optString("provider_name").equalsIgnoreCase("null")) {
            lblProviderDesc.setVisibility(View.GONE);
        } else {
            lblProviderDesc.setVisibility(View.VISIBLE);
            lblProviderDesc.setText(jsonObject.optString("provider_name"));
        }
    }

    public void setValuesForApproximateLayout() {
        if (isInternet) {
            String surge = SharedHelper.getKey(context, "surge");
            if (surge.equalsIgnoreCase("1")) {
                surgeDiscount.setVisibility(View.VISIBLE);
                surgeTxt.setVisibility(View.VISIBLE);
                surgeDiscount.setText(SharedHelper.getKey(context, "surge_value"));
            } else {
                surgeDiscount.setVisibility(View.GONE);
                surgeTxt.setVisibility(View.GONE);
            }
            lblApproxAmount.setText(SharedHelper.getKey(context, "currency") + "" + SharedHelper.getKey(context, "estimated_fare"));
            service_desc.setText(SharedHelper.getKey(context, "service_desc"));
            service_desc2.setText(SharedHelper.getKey(context, "service_desc"));
            service_desc3.setText(SharedHelper.getKey(context, "service_desc"));
//            lblEta.setText(SharedHelper.getKey(context, "eta_time"));
            if (!SharedHelper.getKey(context, "name").equalsIgnoreCase("")
                    && !SharedHelper.getKey(context, "name").equalsIgnoreCase(null)
                    && !SharedHelper.getKey(context, "name").equalsIgnoreCase("null")) {
//                lblType.setText(SharedHelper.getKey(context, "name"));
            } else {
//                lblType.setText("" + "Sedan");
            }


            customDialog.dismiss();
        }
    }

    private void getCards() {
        Ion.with(context)
                .load(URLHelper.CARD_PAYMENT_LIST)
                .addHeader("X-Requested-With", "XMLHttpRequest")
                .addHeader("Authorization", SharedHelper.getKey(context, "token_type") + " " + SharedHelper.getKey(context, "access_token"))
                .asString()
                .withResponse()
                .setCallback(new FutureCallback<com.koushikdutta.ion.Response<String>>() {
                    @Override
                    public void onCompleted(Exception e, com.koushikdutta.ion.Response<String> response) {
                        // response contains both the headers and the string result
                        try {
                            if (response.getHeaders().code() == 200) {
                                try {
                                    JSONArray jsonArray = new JSONArray(response.getResult());
                                    if (jsonArray.length() > 0) {
                                        CardInfo cardInfo = new CardInfo();
                                        cardInfo.setCardId("CASH");
                                        cardInfo.setCardType("CASH");
                                        cardInfo.setLastFour("CASH");
                                        cardInfoArrayList.add(cardInfo);
                                        for (int i = 0; i < jsonArray.length(); i++) {
                                            JSONObject cardObj = jsonArray.getJSONObject(i);
                                            cardInfo = new CardInfo();
                                            cardInfo.setCardId(cardObj.optString("card_id"));
                                            cardInfo.setCardType(cardObj.optString("brand"));
                                            cardInfo.setLastFour(cardObj.optString("last_four"));
                                            cardInfoArrayList.add(cardInfo);
                                        }
                                    }

                                } catch (JSONException e1) {
                                    e1.printStackTrace();
                                }
                            }
                        } catch (Exception e2) {
                            e2.printStackTrace();
                            CardInfo cardInfo = new CardInfo();
                            cardInfo.setCardId("CASH");
                            cardInfo.setCardType("CASH");
                            cardInfo.setLastFour("CASH");
                            cardInfoArrayList.add(cardInfo);
                        }
                    }
                });

    }


    public void getServiceList() {
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(URLHelper.GET_PUBLIC_SERVICE_LIST + 0, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                getServiceProgress.setVisibility(View.GONE);
                utils.print("GetServices", response.toString());
                if (SharedHelper.getKey(context, "service_type").equalsIgnoreCase("")) {
                    SharedHelper.putKey(context, "service_type", "" + response.optJSONObject(0).optString("id"));
                }
//                customDialog.dismiss();
                if (response.length() > 0) {
                    currentPostion = 0;
                    ServiceListAdapter serviceListAdapter = new ServiceListAdapter(response);
                    rcvServiceTypes.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
                    rcvServiceTypes.setAdapter(serviceListAdapter);
//                    getProvidersList(SharedHelper.getKey(context, "service_type"));
                } else {
                    utils.displayMessage(getView(), getString(R.string.no_service));
                }
//                mMap.clear();
//                setValuesForSourceAndDestination();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customDialog.dismiss();
                String json = null;
                String Message;
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {

                    try {
                        JSONObject errorObj = new JSONObject(new String(response.data));

                        if (response.statusCode == 400 || response.statusCode == 405 || response.statusCode == 500) {
                            try {
                                utils.displayMessage(getView(), errorObj.optString("message"));
                            } catch (Exception e) {
                                utils.displayMessage(getView(), getString(R.string.something_went_wrong));
                            }
                            flowValue = 0;
                            layoutChanges();
                        } else if (response.statusCode == 401) {
                            refreshAccessToken("SERVICE_LIST");
                        } else if (response.statusCode == 422) {

                            json = trimMessage(new String(response.data));
                            if (json != "" && json != null) {
                                utils.displayMessage(getView(), json);
                            } else {
                                utils.displayMessage(getView(), getString(R.string.please_try_again));
                            }
                            flowValue = 0;
                            layoutChanges();
                        } else if (response.statusCode == 503) {
                            utils.displayMessage(getView(), getString(R.string.server_down));
                            flowValue = 0;
                            layoutChanges();
                        } else {
                            utils.displayMessage(getView(), getString(R.string.please_try_again));
                            flowValue = 0;
                            layoutChanges();
                        }

                    } catch (Exception e) {
                        utils.displayMessage(getView(), getString(R.string.something_went_wrong));
                        flowValue = 0;
                        layoutChanges();
                    }

                } else {
                    utils.displayMessage(getView(), getString(R.string.please_try_again));
                    flowValue = 0;
                    layoutChanges();
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("X-Requested-With", "XMLHttpRequest");
                headers.put("Authorization", "" + SharedHelper.getKey(context, "token_type") + " "
                        + SharedHelper.getKey(context, "access_token"));
                return headers;
            }
        };

        DocHajj.getInstance().addToRequestQueue(jsonArrayRequest);
    }

    public void getApproximateFare() {
//        customDialog = new CustomDialog(context);
//        customDialog.setCancelable(false);
//        customDialog.show();

        String s_msgURI = "a";
        if (s_gender.equalsIgnoreCase("Any")) {
            s_msgURI = "a";
        } else if (s_gender.equalsIgnoreCase("Female")) {
            s_msgURI = "f";
        } else if (s_gender.equalsIgnoreCase("Male")) {
            s_msgURI = "m";
        }


        JSONObject object = new JSONObject();
        String constructedURL = URLHelper.ESTIMATED_FARE_DETAILS_API + ""
                + "?s_latitude=" + source_lat
                + "&s_longitude=" + source_lng
                + "&s_gender=" + "a"
                + "&s_hours=" + "0"
                + "&s_msg=" + "n/a"
                + "&d_latitude=" + source_lat
                + "&d_longitude=" + source_lng
                + "&service_type=" + SharedHelper.getKey(context, "service_type");

        String SERVICE_DESC = SharedHelper.getKey(context, "name") + ", " + s_gender + ", " + s_hours + " Hrs.";
        SharedHelper.putKey(context, "service_desc", SERVICE_DESC);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, constructedURL, object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
//                customDialog.dismiss();
                if (response != null) {
                    Log.e("Surge", response.toString());
                    if (!response.optString("estimated_fare").equalsIgnoreCase("")) {
                        SharedHelper.putKey(context, "estimated_fare", response.optString("estimated_fare"));
                        SharedHelper.putKey(context, "distance", response.optString("distance"));
                        SharedHelper.putKey(context, "eta_time", response.optString("time"));
                        SharedHelper.putKey(context, "surge", response.optString("surge"));
                        SharedHelper.putKey(context, "surge_value", response.optString("surge_value"));

                        Log.e("ThereIsSurge", response.optString("surge_value"));
                        haveSurge = response.optString("surge");
                        surgeValue = response.optString("surge_value");
//                        setValuesForApproximateLayout();
//                        double wallet_balance = response.optDouble("wallet_balance");
//                        if (!Double.isNaN(wallet_balance) && wallet_balance > 0) {
//                            lineView.setVisibility(View.VISIBLE);
//                            chkWallet.setVisibility(View.VISIBLE);
//                        } else {
//                            lineView.setVisibility(View.GONE);
//                            chkWallet.setVisibility(View.GONE);
//                        }
//                        flowValue = 2;
//                        layoutChanges();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.d(TAG, "onErrorResponse: " + error.toString());
//                customDialog.dismiss();
                String json = null;
                String Message;
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    try {

                        JSONObject errorObj = new JSONObject(new String(response.data));


                        if (response.statusCode == 405 || response.statusCode == 500) {
                            try {
                                utils.showAlert(context, errorObj.optString("message"));
                            } catch (Exception e) {
                                utils.showAlert(context, context.getString(R.string.something_went_wrong));
                            }
                        } else if (response.statusCode == 401) {
                            refreshAccessToken("APPROXIMATE_RATE");
                        } else if (response.statusCode == 422) {
                            json = trimMessage(new String(response.data));
                            if (json != "" && json != null) {
                                utils.showAlert(context, json);
                            } else {
                                utils.showAlert(context, context.getString(R.string.please_try_again));
                            }
                        } else if (response.statusCode == 503) {
                            utils.showAlert(context, context.getString(R.string.server_down));
                        } else if (response.statusCode == 400) {
                            utils.showAlert(context, getString(R.string.oops_connect_your_internet));
                        } else {
                            utils.showAlert(context, context.getString(R.string.please_try_again));
                        }

                    } catch (Exception e) {
                        utils.showAlert(context, context.getString(R.string.something_went_wrong));
                    }

                } else {
                    utils.showAlert(context, context.getString(R.string.please_try_again));
                }

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("X-Requested-With", "XMLHttpRequest");
                headers.put("Authorization", "" + SharedHelper.getKey(context, "token_type") + " " + SharedHelper.getKey(context, "access_token"));
                return headers;
            }
        };

        DocHajj.getInstance().addToRequestQueue(jsonObjectRequest);

    }

    void getProvidersList(String strTag) {

        String providers_request = URLHelper.GET_PROVIDERS_LIST_API + "?" +
                "latitude=" + current_lat +
                "&longitude=" + current_lng +
                "&service=" + strTag;

        utils.print("Get all providers", "" + providers_request);
        utils.print("service_type", "" + SharedHelper.getKey(context, "service_type"));

        for (int i = 0; i < lstProviderMarkers.size(); i++) {
            lstProviderMarkers.get(i).remove();
        }

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(providers_request, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

//                lnrRequestProvidersIN.setVisibility(View.VISIBLE);
//                select_service.setVisibility(View.GONE);
                utils.print("GetProvidersList", response.toString());
//                customDialog.dismiss();
//                mMap.clear();
//
//                setValuesForSourceAndDestination();

                LatLngBounds.Builder builder = new LatLngBounds.Builder();

                for (int i = 0; i < response.length(); i++) {

                    try {
                        JSONObject jsonObj = response.getJSONObject(i);
                        if (!jsonObj.getString("latitude").equalsIgnoreCase("") && !jsonObj.getString("longitude").equalsIgnoreCase("")) {

                            Double proLat = Double.parseDouble(jsonObj.getString("latitude"));
                            Double proLng = Double.parseDouble(jsonObj.getString("longitude"));


                            Float rotation = 0.0f;

//                            MarkerOptions markerOptions = new MarkerOptions()
//                                    .position(new LatLng(proLat, proLng))
//                                    .rotation(rotation)
//                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.provider_marker));

//                        availableProviders = mMap.addMarker(markerOptions);
//                            lstProviderMarkers.add(mMap.addMarker(markerOptions));

                            builder.include(new LatLng(proLat, proLng));

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                CameraUpdate cu = null;
                LatLngBounds bounds = builder.build();

//                cu = CameraUpdateFactory.newLatLngBounds(bounds, mapLayout.getWidth(), mapLayout.getWidth(), context.getResources()
//                        .getDimensionPixelSize(R.dimen._50sdp));
//                mMap.moveCamera(cu);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                lnrRequestProvidersIN.setVisibility(View.GONE);
//                select_service.setVisibility(View.GONE);
//                customDialog.dismiss();
                String json = null;
                String Message;
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {

                    try {
                        JSONObject errorObj = new JSONObject(new String(response.data));

                        if (response.statusCode == 400 || response.statusCode == 405 || response.statusCode == 500) {
                            try {
//                                utils.showAlert(context, errorObj.optString("message"));
                            } catch (Exception e) {
//                                utils.showAlert(context, getString(R.string.something_went_wrong));

                            }

                        } else if (response.statusCode == 401) {
                            refreshAccessToken("PAST_TRIPS");
                        } else if (response.statusCode == 422) {
                            json = trimMessage(new String(response.data));
                            if (json != "" && json != null) {
//                                utils.showAlert(context, json);
                            } else {
//                                utils.showAlert(context, context.getString(R.string.please_try_again));
                            }
                        } else if (response.statusCode == 503) {
//                            utils.showAlert(context, context.getString(R.string.please_try_again));
                        } else {
//                            utils.showAlert(context, context.getString(R.string.please_try_again));
                        }

                    } catch (Exception e) {
//                        utils.showAlert(context, context.getString(R.string.something_went_wrong));

                    }

                } else {
//                    utils.showAlert(context, context.getString(R.string.no_drivers_found));
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("X-Requested-With", "XMLHttpRequest");
                headers.put("Authorization", "Bearer " + SharedHelper.getKey(context, "access_token"));
                return headers;
            }
        };

        DocHajj.getInstance().addToRequestQueue(jsonArrayRequest);

    }

    public void sendRequest() {

        customDialog = new CustomDialog(context);
        customDialog.setCancelable(false);
        customDialog.show();

        Log.e("SENDREQUEST", "SEND REQUEST CALLED");
//
//        String s_msgURI = "a";
//        if (s_gender.equalsIgnoreCase("Any")) {
//            s_msgURI = "a";
//        } else if (s_gender.equalsIgnoreCase("Female")) {
//            s_msgURI = "f";
//        } else if (s_gender.equalsIgnoreCase("Male")) {
//            s_msgURI = "m";
//        }


        JSONObject object = new JSONObject();
        try {
            object.put("s_msg", "n/a");
            object.put("s_hours", "0");
            object.put("s_gender", "a");
            object.put("s_latitude", source_lat);
            object.put("s_longitude", source_lng);
            object.put("d_latitude", source_lat);
            object.put("d_longitude", source_lng);
            object.put("s_address", source_address);
            object.put("d_address", source_address);
            object.put("service_type", "1");
            object.put("distance", "10");
            if (isChat) {
                object.put("is_chat", 1);
            }

            Log.e("Schedule Request", "sendRequest: " + object);

            if (chkWallet.isChecked()) {
                object.put("use_wallet", 1);
            } else {
                object.put("use_wallet", 0);
            }
            if (SharedHelper.getKey(getActivity().getApplicationContext(), "useWallet").equals("yes")) {
                object.put("use_wallet", 1);
            } else {
                object.put("use_wallet", 0);
            }
            if (SharedHelper.getKey(getActivity().getApplicationContext(), "payment_mode").equals("CASH")) {
                object.put("payment_mode", SharedHelper.getKey(context, "payment_mode"));
            } else {
                object.put("payment_mode", SharedHelper.getKey(context, "payment_mode"));
                object.put("card_id", SharedHelper.getKey(context, "card_id"));
            }
            utils.print("SendRequestInput", "" + object.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        DocHajj.getInstance().cancelRequestInQueue("send_request");
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, URLHelper.SEND_REQUEST_API, object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (response != null) {
                    utils.print("SendRequestResponse", response.toString());
                    customDialog.dismiss();
                    if (response.optString("request_id", "").equals("")) {
                        utils.displayMessage(getView(), response.optString("message"));
                    } else {
                        SharedHelper.putKey(context, "current_status", "");
                        SharedHelper.putKey(context, "request_id", "" + response.optString("request_id"));
                        if (mSelectedDates != null && mSelectedDates.length > 0) {
                            scheduleTrip = true;
                        }
                        flowValue = 3;
                        layoutChanges();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customDialog.dismiss();
                String json = null;
                String Message;
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    try {
                        JSONObject errorObj = new JSONObject(new String(response.data));
                        Log.e("SendREquest", "onErrorResponse: " + errorObj.toString());
                        if (response.statusCode == 400 || response.statusCode == 405 || response.statusCode == 500) {
                            try {
                                utils.showAlert(context, errorObj.optString("error"));
                            } catch (Exception e) {
                                utils.showAlert(context, context.getString(R.string.something_went_wrong));
                            }
                        } else if (response.statusCode == 401) {
                            refreshAccessToken("SEND_REQUEST");
                        } else if (response.statusCode == 422) {
                            json = trimMessage(new String(response.data));
                            if (json != "" && json != null) {
                                utils.showAlert(context, json);
                            } else {
                                utils.showAlert(context, context.getString(R.string.please_try_again));
                            }
                        } else if (response.statusCode == 503) {
                            utils.showAlert(context, context.getString(R.string.server_down));
                        } else {
                            utils.showAlert(context, context.getString(R.string.please_try_again));
                        }
                    } catch (Exception e) {
                        utils.showAlert(context, context.getString(R.string.something_went_wrong));
                    }
                } else {
                    utils.showAlert(context, context.getString(R.string.please_try_again));
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("X-Requested-With", "XMLHttpRequest");
                headers.put("Authorization", "" + SharedHelper.getKey(context, "token_type") + " " + SharedHelper.getKey(context, "access_token"));
                return headers;
            }
        };

        DocHajj.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    public void cancelRequest() {
        customDialog = new CustomDialog(context);
        customDialog.setCancelable(false);
        customDialog.show();
        JSONObject object = new JSONObject();
        try {
            object.put("request_id", SharedHelper.getKey(context, "request_id"));
            object.put("cancel_reason", cancalReason);
        } catch (Exception e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, URLHelper.CANCEL_REQUEST_API, object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                utils.print("CancelRequestResponse", response.toString());
                Toast.makeText(context, getResources().getString(R.string.request_cancel), Toast.LENGTH_SHORT).show();
                customDialog.dismiss();
                mapClear();
                SharedHelper.putKey(context, "request_id", "");
                flowValue = 0;
                PreviousStatus = "";
                layoutChanges();
                setupMap();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customDialog.dismiss();
                try {
                    String json = null;
                    String Message;
                    NetworkResponse response = error.networkResponse;
                    if (response != null && response.data != null) {
                        flowValue = 4;
                        try {
                            JSONObject errorObj = new JSONObject(new String(response.data));

                            if (response.statusCode == 400 || response.statusCode == 405 || response.statusCode == 500) {
                                try {
                                    utils.displayMessage(getView(), errorObj.optString("message"));
                                } catch (Exception e) {
                                    utils.displayMessage(getView(), getString(R.string.something_went_wrong));
                                }
                                layoutChanges();
                            } else if (response.statusCode == 401) {
                                refreshAccessToken("CANCEL_REQUEST");
                            } else if (response.statusCode == 422) {

                                json = trimMessage(new String(response.data));
                                if (json != "" && json != null) {
                                    utils.displayMessage(getView(), json);
                                } else {
                                    utils.displayMessage(getView(), getString(R.string.please_try_again));
                                }
                                layoutChanges();
                            } else if (response.statusCode == 503) {
                                utils.displayMessage(getView(), getString(R.string.server_down));
                                layoutChanges();
                            } else {
                                utils.displayMessage(getView(), getString(R.string.please_try_again));
                                layoutChanges();
                            }

                        } catch (Exception e) {
                            utils.displayMessage(getView(), getString(R.string.something_went_wrong));
                            layoutChanges();
                        }

                    } else {
                        utils.displayMessage(getView(), getString(R.string.please_try_again));
                        layoutChanges();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("X-Requested-With", "XMLHttpRequest");
                headers.put("Authorization", "" + SharedHelper.getKey(context, "token_type") + " " + SharedHelper.getKey(context, "access_token"));
                return headers;
            }
        };
//
//        flowValue = 1;
//        layoutChanges();
        value = 0;
        DocHajj.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    public void setValuesForSourceAndDestination() {
        if (isInternet) {
            if (!source_lat.equalsIgnoreCase("")) {
                if (!source_address.equalsIgnoreCase("")) {
                    AppHelper.setDest(destination, source_address);

//                    source.setText(source_address);
                } else {
//                    source.setText(current_address);
                }
            } else {
                AppHelper.setDest(destination, source_address);

//                source.setText(current_address);
            }

            if (!dest_lat.equalsIgnoreCase("")) {
                AppHelper.setDest(destination, source_address);
//                source.setText(source_address);
            }


            if (!source_lat.equalsIgnoreCase("") && !source_lng.equalsIgnoreCase("")) {
                sourceLatLng = new LatLng(Double.parseDouble(source_lat), Double.parseDouble(source_lng));
            }
            if (!dest_lat.equalsIgnoreCase("") && !dest_lng.equalsIgnoreCase("")) {
                destLatLng = new LatLng(Double.parseDouble(dest_lat), Double.parseDouble(dest_lng));
                sourceLatLng = new LatLng(Double.parseDouble(dest_lat), Double.parseDouble(dest_lng));

            }

            if (sourceLatLng != null) {
                utils.print("LatLng", "Source:" + sourceLatLng + " Destination: " + destLatLng);
                String url = getDirectionsUrl(destLatLng, destLatLng);
                DownloadTask downloadTask = new DownloadTask();
                // Start downloading json data from Google Directions API
                downloadTask.execute(url);
            }

        }
    }

    private void refreshAccessToken(final String tag) {


        JSONObject object = new JSONObject();
        try {

            object.put("grant_type", "refresh_token");
            object.put("client_id", URLHelper.client_id);
            object.put("client_secret", URLHelper.client_secret);
            object.put("refresh_token", SharedHelper.getKey(context, "refresh_token"));
            object.put("scope", "");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, URLHelper.login, object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                utils.print("SignUpResponse", response.toString());
                SharedHelper.putKey(context, "access_token", response.optString("access_token"));
                SharedHelper.putKey(context, "refresh_token", response.optString("refresh_token"));
                SharedHelper.putKey(context, "token_type", response.optString("token_type"));
                if (tag.equalsIgnoreCase("SERVICE_LIST")) {
                    getServiceList();
                } else if (tag.equalsIgnoreCase("APPROXIMATE_RATE")) {
                    getApproximateFare();
                } else if (tag.equalsIgnoreCase("SEND_REQUEST")) {
                    sendRequest();
                } else if (tag.equalsIgnoreCase("CANCEL_REQUEST")) {
                    cancelRequest();
                } else if (tag.equalsIgnoreCase("PROVIDERS_LIST")) {
                    getProvidersList("");
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    String json = "";
                    NetworkResponse response = error.networkResponse;

                    if (response != null && response.data != null) {
                        SharedHelper.putKey(context, "loggedIn", getString(R.string.False));
                        utils.GoToBeginActivity(getActivity());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("X-Requested-With", "XMLHttpRequest");
                return headers;
            }
        };
        DocHajj.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    private String getDirectionsUrl(LatLng sourceLatLng, LatLng destLatLng) {

        // Origin of routelng;
        String str_origin = "origin=" + source_lat + "," + source_lng;
        String str_dest = "destination=" + source_lat + "," + source_lng;
        // Sensor enabled
        String sensor = "sensor=false";
        // Waypoints
        String waypoints = "";
        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + waypoints;
        // Output format
        String output = "json";
        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;
        utils.print("url", url.toString());
        return url;

    }

    private void gotoAddCard() {
        Intent mainIntent = new Intent(activity, AddCard.class);
        startActivityForResult(mainIntent, ADD_CARD_CODE);
    }

    private void showChooser() {

        String[] cardsList = new String[cardInfoArrayList.size()];

        for (int i = 0; i < cardInfoArrayList.size(); i++) {
            if (cardInfoArrayList.get(i).getLastFour().equals("CASH")) {
                cardsList[i] = "CASH";
            } else {
                cardsList[i] = "XXXX-XXXX-XXXX-" + cardInfoArrayList.get(i).getLastFour();
            }
        }

        final AlertDialog.Builder builderSingle = new AlertDialog.Builder(context, R.style.MyDialogTheme);
        builderSingle.setTitle(getString(R.string.choose_payment));
        builderSingle.setSingleChoiceItems(cardsList, 0, null);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, R.layout.custom_tv);
        for (int j = 0; j < cardInfoArrayList.size(); j++) {
            String card;
            if (cardInfoArrayList.get(j).getLastFour().equals("CASH")) {
                card = "CASH";
            } else {
                card = "XXXX-XXXX-XXXX-" + cardInfoArrayList.get(j).getLastFour();
            }
            arrayAdapter.add(card);
        }
        builderSingle.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                int selectedPosition = ((AlertDialog) dialog).getListView().getCheckedItemPosition();
                utils.print("Items clicked===>", "" + selectedPosition);
                getCardDetailsForPayment(cardInfoArrayList.get(selectedPosition));
                dialog.dismiss();
            }
        });
        builderSingle.setNegativeButton(
                "cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
//        builderSingle.setAdapter(
//                arrayAdapter,
//                new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        getCardDetailsForPayment(cardInfoArrayList.get(which));
//                        dialog.dismiss();
//                    }
//                });
        builderSingle.show();
    }

    private void getCardDetailsForPayment(CardInfo cardInfo) {

        if (cardInfo.getLastFour().equals("CASH")) {
            SharedHelper.putKey(context, "payment_mode", "CASH");
//            imgPaymentType.setImageResource(R.drawable.money1);
            lblPaymentType.setText("CASH");
        } else {
            SharedHelper.putKey(context, "card_id", cardInfo.getCardId());
            SharedHelper.putKey(context, "payment_mode", "CARD");
//            imgPaymentType.setImageResource(R.drawable.visa);
            lblPaymentType.setText("XXXX-XXXX-XXXX-" + cardInfo.getLastFour());
        }
    }

    public void payNow() {

        customDialog = new CustomDialog(context);
        customDialog.setCancelable(false);
        customDialog.show();

        JSONObject object = new JSONObject();
        try {
            object.put("request_id", SharedHelper.getKey(context, "request_id"));
            object.put("payment_mode", paymentMode);
            object.put("is_paid", isPaid);
        } catch (Exception e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, URLHelper.PAY_NOW_API, object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                utils.print("PayNowRequestResponse", response.toString());
                customDialog.dismiss();
                flowValue = 6;
                layoutChanges();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customDialog.dismiss();
                String json = "";
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    try {
                        JSONObject errorObj = new JSONObject(new String(response.data));

                        if (response.statusCode == 400 || response.statusCode == 405 || response.statusCode == 500) {
                            try {
                                utils.displayMessage(getView(), errorObj.optString("message"));
                            } catch (Exception e) {
                                utils.displayMessage(getView(), getString(R.string.something_went_wrong));
                            }
                        } else if (response.statusCode == 401) {
                            refreshAccessToken("SEND_REQUEST");
                        } else if (response.statusCode == 422) {

                            json = trimMessage(new String(response.data));
                            if (json != "" && json != null) {
                                utils.displayMessage(getView(), json);
                            } else {
                                utils.displayMessage(getView(), getString(R.string.please_try_again));
                            }
                        } else if (response.statusCode == 503) {
                            utils.displayMessage(getView(), getString(R.string.server_down));
                        } else {
                            utils.displayMessage(getView(), getString(R.string.please_try_again));
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        utils.displayMessage(getView(), getString(R.string.something_went_wrong));
                    }

                } else {
                    utils.displayMessage(getView(), getString(R.string.please_try_again));
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", "" + SharedHelper.getKey(context, "token_type") + " " + SharedHelper.getKey(context, "access_token"));
                headers.put("X-Requested-With", "XMLHttpRequest");
                return headers;
            }
        };

        DocHajj.getInstance().addToRequestQueue(jsonObjectRequest);

    }

    private void checkStatus() {
        try {

            utils.print("Handler", "Inside");
            if (isInternet) {
                final JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                        URLHelper.REQUEST_STATUS_CHECK_API, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        utils.print("Response", "" + response.toString());

                        if (response.optJSONArray("data") != null && response.optJSONArray("data").length() > 0) {
                            utils.print("response", "not null");
                            try {
                                JSONArray requestStatusCheck = response.optJSONArray("data");
                                JSONObject requestStatusCheckObject = requestStatusCheck.getJSONObject(0);
                                //Driver Detail
                                if (requestStatusCheckObject.optJSONObject("provider") != null) {
                                    driver = new Driver();
                                    driver.setFname(requestStatusCheckObject.optJSONObject("provider").optString("first_name"));
                                    driver.setLname(requestStatusCheckObject.optJSONObject("provider").optString("last_name"));
                                    driver.setEmail(requestStatusCheckObject.optJSONObject("provider").optString("email"));
                                    driver.setMobile(requestStatusCheckObject.optJSONObject("provider").optString("mobile"));
                                    driver.setImg(requestStatusCheckObject.optJSONObject("provider").optString("avatar"));
                                    driver.setRating(requestStatusCheckObject.optJSONObject("provider").optString("rating"));
                                }
                                String status = requestStatusCheckObject.optString("status");
                                String wallet = requestStatusCheckObject.optString("use_wallet");
                                source_lat = requestStatusCheckObject.optString("s_latitude");
                                source_lng = requestStatusCheckObject.optString("s_longitude");
//                                dest_lat = requestStatusCheckObject.optString("d_latitude");
//                                dest_lng = requestStatusCheckObject.optString("d_longitude");

                                if (!source_lat.equalsIgnoreCase("") && !source_lng.equalsIgnoreCase("")) {
                                    LatLng myLocation = new LatLng(Double.parseDouble(source_lat), Double.parseDouble(source_lng));
                                    CameraPosition cameraPosition = new CameraPosition.Builder().target(myLocation).zoom(16).build();
                                }


                                // surge price
                                if (requestStatusCheckObject.optString("surge").equalsIgnoreCase("1")) {
                                    lblSurgePrice.setVisibility(View.VISIBLE);
                                } else {
                                    lblSurgePrice.setVisibility(View.GONE);
                                }

                                utils.print("PreviousStatus", "" + PreviousStatus);
                                service_desc3.setText(SharedHelper.getKey(context, "service_desc"));
                                if (!PreviousStatus.equals(status)) {
                                    mMap.clear();
                                    PreviousStatus = status;
                                    flowValue = 8;
                                    layoutChanges();
                                    SharedHelper.putKey(context, "request_id", "" + requestStatusCheckObject.optString("id"));
                                    reCreateMap();
                                    utils.print("ResponseStatus", "SavedCurrentStatus: " + CurrentStatus + " Status: " + status);
                                    switch (status) {
                                        case "SEARCHING":
                                            show(lnrWaitingForProviders);
                                            //rippleBackground.startRippleAnimation();
                                            strTag = "search_completed";
                                            if (!source_lat.equalsIgnoreCase("") && !source_lng.equalsIgnoreCase("")) {
                                                LatLng myLocation1 = new LatLng(Double.parseDouble(source_lat),
                                                        Double.parseDouble(source_lng));
                                                CameraPosition cameraPosition1 = new CameraPosition.Builder().target(myLocation1).zoom(14).build();
                                                mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition1));
                                            }
                                            break;
                                        case "CANCELLED":
                                            strTag = "";
                                            imgSos.setVisibility(View.GONE);
                                            break;
                                        case "ACCEPTED":
                                            strTag = "ride_accepted";
                                            try {
                                                JSONObject provider = requestStatusCheckObject.getJSONObject("provider");
                                                JSONObject service_type = requestStatusCheckObject.getJSONObject("service_type");
                                                JSONObject provider_service = requestStatusCheckObject.getJSONObject("provider_service");
                                                SharedHelper.putKey(context, "provider_mobile_no", "" + provider.optString("mobile"));
                                                lblProvider.setText(provider.optString("first_name") + " " + provider.optString("last_name"));
                                                if (provider.optString("avatar").startsWith("http"))

                                                    Picasso.with(context).load(provider.optString("avatar")).placeholder(R.drawable.ic_dummy_user).error(R.drawable.ic_dummy_user).into(imgProvider);

                                                else

                                                    Picasso.with(context).load(URLHelper.base + "storage/" + provider.optString("avatar")).placeholder(R.drawable.ic_dummy_user).error(R.drawable.ic_dummy_user).into(imgProvider);
//                                                Picasso.with(context).load(service_type.optString("image")).placeholder(R.drawable.car_select).error(R.drawable.car_select).into(imgServiceRequested);
                                                ratingProvider.setRating(Float.parseFloat(provider.optString("rating")));

                                                serviceStateChange(1);


                                                AfterAcceptButtonLayout.setVisibility(View.VISIBLE);
                                                lnrRequestProviders.setVisibility(View.GONE);
                                                show(lnrProviderAccepted);
                                                flowValue = 4;
                                                layoutChanges();
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            break;
                                        case "STARTED":
                                            strTag = "ride_started";
                                            try {
                                                JSONObject provider = requestStatusCheckObject.getJSONObject("provider");
                                                JSONObject service_type = requestStatusCheckObject.getJSONObject("service_type");
                                                JSONObject provider_service = requestStatusCheckObject.getJSONObject("provider_service");
                                                SharedHelper.putKey(context, "provider_mobile_no", "" + provider.optString("mobile"));
                                                lblProvider.setText(provider.optString("first_name") + " " + provider.optString("last_name"));
                                                if (provider.optString("avatar").startsWith("http"))
                                                    Picasso.with(context).load(provider.optString("avatar")).placeholder(R.drawable.ic_dummy_user).error(R.drawable.ic_dummy_user).into(imgProvider);
                                                else
                                                    Picasso.with(context).load(URLHelper.base + "storage/" + provider.optString("avatar")).placeholder(R.drawable.ic_dummy_user).error(R.drawable.ic_dummy_user).into(imgProvider);
//                                                lblServiceRequested.setText(service_type.optString("name"));
//                                                lblModelNumber.setText(provider_service.optString("service_model") + "\n" + provider_service.optString("service_number"));
//                                                Picasso.with(context).load(service_type.optString("image")).placeholder(R.drawable.car_select)
//                                                        .error(R.drawable.car_select).into(imgServiceRequested);
                                                ratingProvider.setRating(Float.parseFloat(provider.optString("rating")));


                                                serviceStateChange(1);


                                                AfterAcceptButtonLayout.setVisibility(View.VISIBLE);
                                                flowValue = 4;
                                                layoutChanges();
                                                if (!requestStatusCheckObject.optString("schedule_at").equalsIgnoreCase("null")) {
                                                    SharedHelper.putKey(context, "current_status", "");
                                                    Intent intent = new Intent(getActivity(), HistoryActivity.class);
                                                    intent.putExtra("tag", "upcoming");
                                                    startActivity(intent);
                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            break;

                                        case "ARRIVED":
                                            once = true;
                                            strTag = "ride_arrived";
                                            utils.print("MyTest", "ARRIVED");
                                            try {
                                                utils.print("MyTest", "ARRIVED TRY");
                                                JSONObject provider = requestStatusCheckObject.getJSONObject("provider");
                                                JSONObject service_type = requestStatusCheckObject.getJSONObject("service_type");
                                                JSONObject provider_service = requestStatusCheckObject.getJSONObject("provider_service");
                                                lblProvider.setText(provider.optString("first_name") + " " + provider.optString("last_name"));
                                                if (provider.optString("avatar").startsWith("http"))
                                                    Picasso.with(context).load(provider.optString("avatar")).placeholder(R.drawable.ic_dummy_user).error(R.drawable.ic_dummy_user).into(imgProvider);
                                                else
                                                    Picasso.with(context).load(URLHelper.base + "storage/" + provider.optString("avatar")).placeholder(R.drawable.ic_dummy_user).error(R.drawable.ic_dummy_user).into(imgProvider);
//                                                lblServiceRequested.setText(service_type.optString("name"));
//                                                lblModelNumber.setText(provider_service.optString("service_model") + "\n" + provider_service.optString("service_number"));
//                                                Picasso.with(context).load(service_type.optString("image")).placeholder(R.drawable.car_select).error(R.drawable.car_select).into(imgServiceRequested);
                                                ratingProvider.setRating(Float.parseFloat(provider.optString("rating")));


                                                serviceStateChange(2);


//                                                tripLine.setVisibility(View.VISIBLE);
//                                                lblStatus.setText(getString(R.string.arrived));
                                                AfterAcceptButtonLayout.setVisibility(View.VISIBLE);
                                                btnCancelTrip.setVisibility(View.GONE);


                                                flowValue = 4;
                                                layoutChanges();
                                            } catch (Exception e) {
                                                utils.print("MyTest", "ARRIVED CATCH");
                                                e.printStackTrace();
                                            }
                                            break;

                                        case "PICKEDUP":
                                            once = true;
                                            strTag = "ride_picked";
                                            try {
                                                JSONObject provider = requestStatusCheckObject.getJSONObject("provider");
                                                JSONObject service_type = requestStatusCheckObject.getJSONObject("service_type");
                                                JSONObject provider_service = requestStatusCheckObject.getJSONObject("provider_service");
                                                lblProvider.setText(provider.optString("first_name") + " " + provider.optString("last_name"));
                                                if (provider.optString("avatar").startsWith("http"))
                                                    Picasso.with(context).load(provider.optString("avatar")).placeholder(R.drawable.ic_dummy_user).error(R.drawable.ic_dummy_user).into(imgProvider);
                                                else
                                                    Picasso.with(context).load(URLHelper.base + "storage/" + provider.optString("avatar")).placeholder(R.drawable.ic_dummy_user).error(R.drawable.ic_dummy_user).into(imgProvider);
//                                                lblServiceRequested.setText(service_type.optString("name"));
//                                                lblModelNumber.setText(provider_service.optString("service_model") + "\n" + provider_service.optString("service_number"));
//                                                Picasso.with(context).load(service_type.optString("image")).placeholder(R.drawable.car_select).error(R.drawable.car_select).into(imgServiceRequested);
                                                ratingProvider.setRating(Float.parseFloat(provider.optString("rating")));


                                                serviceStateChange(3);


//                                                tripLine.setVisibility(View.VISIBLE);
                                                imgSos.setVisibility(View.VISIBLE);
                                                //imgShareRide.setVisibility(View.VISIBLE);
//                                                lblStatus.setText(getString(R.string.picked_up));
//                                                btnCancelTrip.setText(getString(R.string.share));
                                                AfterAcceptButtonLayout.setVisibility(View.VISIBLE);


                                                flowValue = 4;
                                                layoutChanges();
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            break;

                                        case "DROPPED":


//                                            once = true;
//                                            strTag = "";
//                                            imgSos.setVisibility(View.GONE);
//                                            //imgShareRide.setVisibility(View.VISIBLE);
//
//                                            serviceStateChange(4);
//
//                                            try {
//                                                JSONObject provider = requestStatusCheckObject.optJSONObject("provider");
//                                                if (requestStatusCheckObject.optJSONObject("payment") != null) {
//                                                    JSONObject payment = requestStatusCheckObject.optJSONObject("payment");
//                                                    isPaid = requestStatusCheckObject.optString("paid");
//                                                    paymentMode = requestStatusCheckObject.optString("payment_mode");
//                                                    //invoice parameter
//                                                    lblBasePrice.setText(SharedHelper.getKey(context, "currency") + "" + payment.optString("fixed"));
//                                                    lblFeeCharge.setText(SharedHelper.getKey(context, "currency") + "" + payment.optString("timefee"));
//                                                    lblInvoiceSurgePrice.setText(SharedHelper.getKey(context, "currency") + "" + payment.optString("surge"));
//                                                    lblExtraPrice.setText(SharedHelper.getKey(context, "currency") + "" + payment.optString("discount"));
//                                                    lblWalletPrice.setText(SharedHelper.getKey(context, "currency") + "" + payment.optString("wallet"));
//                                                    lblTaxPrice.setText(SharedHelper.getKey(context, "currency") + "" + payment.optString("tax"));
////                                                    lblDistancePrice.setText(SharedHelper.getKey(context, "currency") + "" + payment.optString("distance"));
//                                                    //lblCommision.setText(SharedHelper.getKey(context, "currency") + "" + payment.optString("commision"));
//
//                                                    int subTotalInt = Integer.parseInt(payment.optString("fixed")) + Integer.parseInt(payment.optString("timefee")) +
//                                                            Integer.parseInt(payment.optString("surge")) + Integer.parseInt(payment.optString("tax"));
//                                                    sbTotal.setText(SharedHelper.getKey(context, "currency") + "" + subTotalInt);
//
//                                                    lblTotalPrice.setText(SharedHelper.getKey(context, "currency") + "" + payment.optString("paid_cash"));
//                                                }
//                                                if (requestStatusCheckObject.optString("booking_id") != null &&
//                                                        !requestStatusCheckObject.optString("booking_id").equalsIgnoreCase("")) {
//                                                    booking_id.setText(requestStatusCheckObject.optString("booking_id"));
//                                                } else {
//                                                    booking_id.setVisibility(View.GONE);
//                                                }
//                                                if (isPaid.equalsIgnoreCase("0") && paymentMode.equalsIgnoreCase("CASH")) {
//                                                    btnPayNow.setVisibility(View.GONE);
//                                                    flowValue = 5;
//                                                    layoutChanges();
//                                                    imgPaymentTypeInvoice.setImageResource(R.drawable.money1);
//                                                    lblPaymentTypeInvoice.setText("CASH");
//                                                } else if (isPaid.equalsIgnoreCase("0") && paymentMode.equalsIgnoreCase("CASH")
//                                                        && wallet.equalsIgnoreCase("1")) {
//                                                    btnPayNow.setVisibility(View.GONE);
//                                                    flowValue = 5;
//                                                    layoutChanges();
//                                                    imgPaymentTypeInvoice.setImageResource(R.drawable.visa);
//                                                    lblPaymentTypeInvoice.setText("CASH AND WALLET");
//                                                } else if (isPaid.equalsIgnoreCase("0") && paymentMode.equalsIgnoreCase("CARD")) {
//                                                    btnPayNow.setVisibility(View.GONE);
//                                                    flowValue = 5;
//                                                    layoutChanges();
//                                                    imgPaymentTypeInvoice.setImageResource(R.drawable.visa);
//                                                    lblPaymentTypeInvoice.setText("CARD");
//                                                } else if (isPaid.equalsIgnoreCase("1")) {
//                                                    btnPayNow.setVisibility(View.GONE);
//                                                    lblProviderNameRate.setText(getString(R.string.rate_provider) + " " + provider.optString("first_name") + " " + provider.optString("last_name"));
//                                                    if (provider.optString("avatar").startsWith("http"))
//                                                        Picasso.with(context).load(provider.optString("avatar")).placeholder(R.drawable.loading).error(R.drawable.ic_dummy_user).into(imgProvider);
//                                                    else
//                                                        Picasso.with(context).load(URLHelper.base + "storage/" + provider.optString("avatar")).placeholder(R.drawable.loading).error(R.drawable.ic_dummy_user).into(imgProvider);
//                                                    flowValue = 6;
//                                                    layoutChanges();
//                                                }
//
//                                            } catch (Exception e) {
//                                                e.printStackTrace();
//                                            }
                                            break;

                                        case "COMPLETED":
                                            strTag = "";
                                            value = 0;

                                            serviceStateChange(4);
                                            try {
                                                if (requestStatusCheckObject.optJSONObject("payment") != null) {
                                                    JSONObject payment = requestStatusCheckObject.optJSONObject("payment");
                                                    //invoice parameter
                                                    lblBasePrice.setText(SharedHelper.getKey(context, "currency") + "" + payment.optString("fixed"));
                                                    lblFeeCharge.setText(SharedHelper.getKey(context, "currency") + "" + payment.optString("timefee"));
                                                    lblInvoiceSurgePrice.setText(SharedHelper.getKey(context, "currency") + "" + payment.optString("surge"));
                                                    lblExtraPrice.setText(SharedHelper.getKey(context, "currency") + "" + payment.optString("discount"));
                                                    lblWalletPrice.setText(SharedHelper.getKey(context, "currency") + "" + payment.optString("wallet"));
                                                    lblTaxPrice.setText(SharedHelper.getKey(context, "currency") + "" + payment.optString("tax"));
//                                                    lblDistancePrice.setText(SharedHelper.getKey(context, "currency") + "" + payment.optString("distance"));
                                                    int subTotalInt = Integer.parseInt(payment.optString("fixed")) + Integer.parseInt(payment.optString("timefee")) +
                                                            Integer.parseInt(payment.optString("surge")) + Integer.parseInt(payment.optString("tax"));
                                                    sbTotal.setText(SharedHelper.getKey(context, "currency") + "" + subTotalInt);

                                                    lblTotalPrice.setText(SharedHelper.getKey(context, "currency") + "" + payment.optString("paid_cash"));
                                                }
                                                JSONObject provider = requestStatusCheckObject.optJSONObject("provider");
                                                isPaid = requestStatusCheckObject.optString("paid");
                                                paymentMode = requestStatusCheckObject.optString("payment_mode");
                                                imgSos.setVisibility(View.GONE);
                                                imgShareRide.setVisibility(View.GONE);
                                                // lblCommision.setText(payment.optString("commision"));
                                                if (isPaid.equalsIgnoreCase("0") && paymentMode.equalsIgnoreCase("CASH")) {
                                                    flowValue = 5;
                                                    layoutChanges();
                                                    btnPayNow.setVisibility(View.GONE);
                                                    imgPaymentTypeInvoice.setImageResource(R.drawable.money1);
                                                    lblPaymentTypeInvoice.setText("CASH");
                                                } else if (isPaid.equalsIgnoreCase("0") && paymentMode.equalsIgnoreCase("CARD")) {
                                                    flowValue = 5;
                                                    layoutChanges();
                                                    imgPaymentTypeInvoice.setImageResource(R.drawable.visa);
                                                    lblPaymentTypeInvoice.setText("CARD");
                                                    btnPayNow.setVisibility(View.GONE);
                                                } else if (isPaid.equalsIgnoreCase("1")) {
                                                    btnPayNow.setVisibility(View.GONE);
                                                    lblProviderNameRate.setText(getString(R.string.rate_provider) + " " + provider.optString("first_name") + " " + provider.optString("last_name"));
                                                    if (provider.optString("avatar").startsWith("http"))
                                                        Picasso.with(context).load(provider.optString("avatar")).placeholder(R.drawable.loading).error(R.drawable.ic_dummy_user).into(imgProviderRate);
                                                    else
                                                        Picasso.with(context).load(URLHelper.base + "storage/" + provider.optString("avatar")).placeholder(R.drawable.loading).error(R.drawable.ic_dummy_user).into(imgProviderRate);
                                                    flowValue = 6;
                                                    layoutChanges();
                                                    //imgPaymentTypeInvoice.setImageResource(R.drawable.visa);
                                                    // lblPaymentTypeInvoice.setText("CARD");
                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            break;
                                    }
                                }

                                if ("SEARCHING".equals(status) || "ACCEPTED".equals(status) ||
                                        "STARTED".equals(status) || "ARRIVED".equals(status) ||
                                        "PICKEDUP".equals(status) || "DROPPED".equals(status) ||
                                        "COMPLETED".equals(status)) {
                                    if ("ACCEPTED".equals(status) || "STARTED".equals(status) ||
                                            "ARRIVED".equals(status) || "PICKEDUP".equals(status) ||
                                            "DROPPED".equals(status) || "COMPLETED".equals(status)) {
                                        utils.print("Livenavigation", "" + status);
                                        utils.print("Destination Current Lat", "" + requestStatusCheckObject.getJSONObject("provider").optString("latitude"));
                                        utils.print("Destination Current Lng", "" + requestStatusCheckObject.getJSONObject("provider").optString("longitude"));
                                        liveNavigation(status, requestStatusCheckObject.getJSONObject("provider").optString("latitude"),
                                                requestStatusCheckObject.getJSONObject("provider").optString("longitude"));
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                utils.displayMessage(getView(), getString(R.string.something_went_wrong));
                            }
                        } else if (PreviousStatus.equalsIgnoreCase("SEARCHING")) {
                            SharedHelper.putKey(context, "current_status", "");
                            if (scheduledDate != null && scheduledTime != null && !scheduledDate.equalsIgnoreCase("")
                                    && !scheduledTime.equalsIgnoreCase("")) {

                                //TODO: update s_lat, s_lng after requests ends

                                Toast.makeText(context, getString(R.string.schdule_accept), Toast.LENGTH_SHORT).show();
                                if (scheduleTrip) {
                                    Intent intent = new Intent(activity, HistoryActivity.class);
                                    intent.putExtra("tag", "upcoming");
                                    startActivity(intent);
                                }

                                mapClear();
                                SharedHelper.putKey(context, "request_id", "");
                                flowValue = 0;
                                PreviousStatus = "";
                                layoutChanges();
                                setupMap();


                            } else {
                                Toast.makeText(context, getString(R.string.no_drivers_found), Toast.LENGTH_SHORT).show();
                                strTag = "";
                                PreviousStatus = "";
                                value = 0;
                                layoutChanges();
                                mMap.clear();
                                mapClear();
                                SharedHelper.putKey(context, "request_id", "");
                                flowValue = 0;
                                PreviousStatus = "";
                                layoutChanges();
                                setupMap();

                            }
                        } else if (PreviousStatus.equalsIgnoreCase("STARTED")) {
                            SharedHelper.putKey(context, "current_status", "");
                            Toast.makeText(context, getString(R.string.driver_busy), Toast.LENGTH_SHORT).show();
                            strTag = "";
                            PreviousStatus = "";
                            flowValue = 0;
                            layoutChanges();
                            mMap.clear();
                            mapClear();
                            SharedHelper.putKey(context, "request_id", "");
                            flowValue = 0;
                            PreviousStatus = "";
                            layoutChanges();
                            setupMap();
                        } else if (PreviousStatus.equalsIgnoreCase("ARRIVED")) {
                            SharedHelper.putKey(context, "current_status", "");
                            Toast.makeText(context, getString(R.string.driver_busy), Toast.LENGTH_SHORT).show();
                            strTag = "";
                            PreviousStatus = "";
                            flowValue = 0;
                            layoutChanges();
                            mMap.clear();
                            mapClear();
                            SharedHelper.putKey(context, "request_id", "");
                            flowValue = 0;
                            PreviousStatus = "";
                            layoutChanges();
                            setupMap();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        utils.print("Error", error.toString());

                    }
                }) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put("X-Requested-With", "XMLHttpRequest");
                        utils.print("Authorization", "" + SharedHelper.getKey(context, "token_type") + " " + SharedHelper.getKey(context, "access_token"));

                        headers.put("Authorization", "" + SharedHelper.getKey(context, "token_type") + " " + SharedHelper.getKey(context, "access_token"));
                        return headers;
                    }
                };

                DocHajj.getInstance().addToRequestQueue(jsonObjectRequest);

            } else {
                utils.displayMessage(getView(), getString(R.string.oops_connect_your_internet));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void serviceStateChange(int i) {
        lngstep1.startAnimation(slide_down);
        lngstep2.startAnimation(slide_down);
        lngstep3.startAnimation(slide_down);
        // hide views
        lngstep1.setVisibility(View.GONE);
        lngstep2.setVisibility(View.GONE);
        lngstep3.setVisibility(View.GONE);


        Log.d("STATUSCALLED", SharedHelper.getKey(context, "service_desc"));
        service_desc3.setText(SharedHelper.getKey(context, "service_desc"));


        switch (i) {
            case 1:
                lngstep1.startAnimation(slide_up);
                lngstep1.setVisibility(View.VISIBLE);
                btnCancelTrip.setVisibility(View.VISIBLE);
                break;
            case 2:
                lngstep2.startAnimation(slide_up);
                lngstep2.setVisibility(View.VISIBLE);
                btnCancelTrip.setVisibility(View.VISIBLE);
                break;
            case 3:
                lngstep3.startAnimation(slide_up);
                lngstep3.setVisibility(View.VISIBLE);
                btnCancelTrip.setVisibility(View.GONE);
                break;
            case 4:
                btnCancelTrip.setVisibility(View.VISIBLE);
                break;
        }

    }


    public void checkPhoneVerfied() {
        String existPhone = SharedHelper.getKey(context, "mob_verfied");
        Log.d("mobverfiedcalled", "mob verfied value " + existPhone);
        if (existPhone.equalsIgnoreCase("0") || existPhone == null) {
            if (helpDialog == 0) {
                Intent validatePhone = new Intent(getActivity(), ActivityMobValidation.class);
                validatePhone.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(validatePhone);
                activity.finish();

                helpDialog++;
            }
        }
    }

    private void mapClear() {
        if (parserTask != null)
            parserTask.cancel(true);
        mMap.clear();
        source_lat = "";
        source_lng = "";
        dest_lat = "";
        dest_lng = "";
        if (!current_lat.equalsIgnoreCase("") && !current_lng.equalsIgnoreCase("")) {
            LatLng myLocation = new LatLng(Double.parseDouble(current_lat), Double.parseDouble(current_lng));
            CameraPosition cameraPosition = new CameraPosition.Builder().target(myLocation).zoom(14).build();
            mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
    }

    public void reCreateMap() {
        if (mMap != null) {
            if (!source_lat.equalsIgnoreCase("") && !source_lng.equalsIgnoreCase("")) {
                sourceLatLng = new LatLng(Double.parseDouble(source_lat), Double.parseDouble(source_lng));
            }
            if (!dest_lat.equalsIgnoreCase("") && !dest_lng.equalsIgnoreCase("")) {
//                destLatLng = new LatLng(Double.parseDouble(dest_lat), Double.parseDouble(dest_lng));
            }
//            utils.print("LatLng", "Source:" + sourceLatLng + " Destination: " + destLatLng);
//            String url = getDirectionsUrl(sourceLatLng, destLatLng);
//            DownloadTask downloadTask = new DownloadTask();
//            downloadTask.execute(url);
        }
    }

    private void show(final View view) {
        mIsShowing = true;
        ViewPropertyAnimator animator = view.animate()
                .translationY(0)
                .setInterpolator(INTERPOLATOR)
                .setDuration(500);

        animator.setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                view.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                mIsShowing = false;
            }

            @Override
            public void onAnimationCancel(Animator animator) {
                // Canceling a show should hide the view
                mIsShowing = false;
                if (!mIsHiding) {
                    hide(view);
                }
            }

            @Override
            public void onAnimationRepeat(Animator animator) {
            }
        });

        animator.start();
    }

    private void hide(final View view) {
        mIsHiding = true;
        ViewPropertyAnimator animator = view.animate()
                .translationY(view.getHeight())
                .setInterpolator(INTERPOLATOR)
                .setDuration(200);

        animator.setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                // Prevent drawing the View after it is gone
                mIsHiding = false;
                view.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(Animator animator) {
                // Canceling a hide should show the view
                mIsHiding = false;
                if (!mIsShowing) {
                    show(view);
                }
            }

            @Override
            public void onAnimationRepeat(Animator animator) {
            }
        });

        animator.start();
    }

    public void liveNavigation(String status, String lat, String lng) {
        Log.e("Livenavigation", "ProLat" + lat + " ProLng" + lng);
        if (!lat.equalsIgnoreCase("") && !lng.equalsIgnoreCase("")) {
            Double proLat = Double.parseDouble(lat);
            Double proLng = Double.parseDouble(lng);

            Float rotation = 0.0f;

            MarkerOptions markerOptions = new MarkerOptions()
                    .position(new LatLng(proLat, proLng))
                    .rotation(rotation)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.provider));

            if (providerMarker != null) {
                rotation = getBearing(providerMarker.getPosition(), markerOptions.getPosition());
                markerOptions.rotation(rotation * (180.0f / (float) Math.PI));
                providerMarker.remove();
            }

            providerMarker = mMap.addMarker(markerOptions);
        }

    }

//    public void liveNavigation(String status, String lat, String lng) {
//       utils.print("Livenavigation", "ProLat" + lat + " ProLng" + lng);
//        Double proLat = Double.parseDouble(lat);
//        Double proLng = Double.parseDouble(lng);
//        Location targetLocation = new Location("");//provider name is unecessary
//        targetLocation.setLatitude(proLat);//your coords of course
//        targetLocation.setLongitude(proLng);
//        if (once) {
//            once = false;
//            LatLng latLng = new LatLng(proLat, proLng);
//            // Showing the current location in Google Map
//            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
//            // Zoom in the Google Map
//            mMap.moveCamera(CameraUpdateFactory.zoomTo(18));
//            LatLng providerPosition = new LatLng(proLat, proLng);
//            MarkerOptions markerOptions = new MarkerOptions();
//            markerOptions.position(providerPosition);
//            providerMarker = mMap.addMarker(markerOptions);
//            providerMarker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.provider_loaction_marker));
//        }
////            animateMarker(providerMarker, providerMarker.getPosition(), false);
//
//        if (status.equalsIgnoreCase("PICKEDUP")) {
//            utils.animateMarker(mMap, targetLocation, providerMarker);
//        } else {
//            providerMarker.setPosition(new LatLng(proLat, proLng));
//        }
//    }

    public float getBearing(LatLng oldPosition, LatLng newPosition) {
        double deltaLongitude = newPosition.longitude - oldPosition.longitude;
        double deltaLatitude = newPosition.latitude - oldPosition.latitude;
        double angle = (Math.PI * .5f) - Math.atan(deltaLatitude / deltaLongitude);

        if (deltaLongitude > 0) {
            return (float) angle;
        } else if (deltaLongitude < 0) {
            return (float) (angle + Math.PI);
        } else if (deltaLatitude < 0) {
            return (float) Math.PI;
        }

        return 0.0f;
    }

    public void statusCheck() {
        try {
            final LocationManager manager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);

            if (manager != null && !manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                enableLoc();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void enableLoc() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(Bundle bundle) {
                        Log.d("Location error", "Connected");
                    }

                    @Override
                    public void onConnectionSuspended(int i) {
                        mGoogleApiClient.connect();
                    }
                })
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(ConnectionResult connectionResult) {

                        Log.d("Location error", "Location error " + connectionResult.getErrorCode());
                    }
                }).build();
        mGoogleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);

        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                try {
                    final Status status = result.getStatus();
                    Log.e("GPS Location", "onResult: " + result);
                    Log.e("GPS Location", "onResult Status: " + result.getStatus());
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            try {
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(getActivity(), REQUEST_LOCATION);
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }
                            break;

                        case LocationSettingsStatusCodes.CANCELED:
                            showDialogForGPSIntent();
                            break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
//	        }

    }

    public void submitReviewCall() {

        customDialog = new CustomDialog(context);
        customDialog.setCancelable(false);
        customDialog.show();


        RateThisApp.Config config = new RateThisApp.Config(5, 5);
        config.setMessage(R.string.my_own_message);
        RateThisApp.init(config);

        RateThisApp.showRateDialogIfNeeded(getContext());

        SharedHelper.putKey(getContext(), "store_rated", "1");


        Log.e("RATING ARRAY:", feedBackRating);
        JSONObject object = new JSONObject();
        try {
            object.put("request_id", SharedHelper.getKey(context, "request_id"));
            object.put("rating", feedBackRating);
            object.put("comment", "" + txtCommentsRate.getText().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, URLHelper.RATE_PROVIDER_API, object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                utils.print("SubmitRequestResponse", response.toString());
                utils.hideKeypad(context, activity.getCurrentFocus());


                customDialog.dismiss();
                mapClear();
                SharedHelper.putKey(context, "request_id", "");
                flowValue = 0;
                PreviousStatus = "";
                layoutChanges();
                setupMap();

                if (!current_lat.equalsIgnoreCase("") && !current_lng.equalsIgnoreCase("")) {
                    LatLng myLocation = new LatLng(Double.parseDouble(current_lat), Double.parseDouble(current_lng));
                    CameraPosition cameraPosition = new CameraPosition.Builder().target(myLocation).zoom(16).build();
                    mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customDialog.dismiss();
                String json = null;
                String Message;
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {

                    try {
                        JSONObject errorObj = new JSONObject(new String(response.data));

                        if (response.statusCode == 400 || response.statusCode == 405 || response.statusCode == 500) {
                            try {
                                utils.displayMessage(getView(), errorObj.optString("message"));
                            } catch (Exception e) {
                                utils.displayMessage(getView(), getString(R.string.something_went_wrong));
                            }

                        } else if (response.statusCode == 401) {
                            refreshAccessToken("SEND_REQUEST");
                        } else if (response.statusCode == 422) {

                            json = trimMessage(new String(response.data));
                            if (json != "" && json != null) {
                                utils.displayMessage(getView(), json);
                            } else {
                                utils.displayMessage(getView(), getString(R.string.please_try_again));
                            }
                        } else if (response.statusCode == 503) {
                            utils.displayMessage(getView(), getString(R.string.server_down));
                        } else {
                            utils.displayMessage(getView(), getString(R.string.please_try_again));
                        }

                    } catch (Exception e) {
                        utils.displayMessage(getView(), getString(R.string.something_went_wrong));
                    }

                } else {
                    utils.displayMessage(getView(), getString(R.string.please_try_again));
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("X-Requested-With", "XMLHttpRequest");
                headers.put("Authorization", "" + SharedHelper.getKey(context, "token_type") + " " + SharedHelper.getKey(context, "access_token"));
                return headers;
            }
        };
        value = 0;


        DocHajj.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    private void startAnim(ArrayList<LatLng> routeList) {
        if (mMap != null && routeList.size() > 1) {
            MapAnimator.getInstance().animateRoute(mMap, routeList);
        } else {
            Toast.makeText(context, "Map not ready", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onDestroy() {
        if (handleCheckStatus != null) {
            handleCheckStatus.removeCallbacksAndMessages(null);
        }
        if (mapRipple != null && mapRipple.isAnimationRunning()) {
            mapRipple.stopRippleMapAnimation();
        }
        if (customDialog != null && customDialog.isShowing()) {
            customDialog.dismiss();
        }


        super.onDestroy();
    }

    private void stopAnim() {
        if (mMap != null) {
            MapAnimator.getInstance().stopAnim();
        } else {
            Toast.makeText(context, "Map not ready", Toast.LENGTH_LONG).show();
        }
    }

    private void showDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(getString(R.string.connect_to_network))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.connect_to_wifi), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                    }
                });
        if (alert == null) {
            alert = builder.create();
            alert.show();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        if (ContextCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public void displayMessage(String msg) {
        helpDialog = 0;
        checkPhoneVerfied();
        utils.displayMessage(getView(), msg);
    }

    public void refreshAT() {

        if (isInternet) {
            customDialog = new CustomDialog(context);
            customDialog.setCancelable(false);
            customDialog.show();
            JSONObject object = new JSONObject();
            try {

                object.put("grant_type", "refresh_token");
                object.put("client_id", URLHelper.client_id);
                object.put("client_secret", URLHelper.client_secret);
                object.put("refresh_token", SharedHelper.getKey(context, "refresh_token"));
                object.put("scope", "");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, URLHelper.login, object, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    customDialog.dismiss();
                    Log.v("SignUpResponse", response.toString());
                    SharedHelper.putKey(context, "access_token", response.optString("access_token"));
                    SharedHelper.putKey(context, "refresh_token", response.optString("refresh_token"));
                    SharedHelper.putKey(context, "token_type", response.optString("token_type"));
//                        getProfile();


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    try {
                        customDialog.dismiss();
                        String json = null;
                        String Message;
                        NetworkResponse response = error.networkResponse;

                        if (response != null && response.data != null) {
                            SharedHelper.putKey(context, "loggedIn", getString(R.string.False));
//                            GoToBeginActivity();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("X-Requested-With", "XMLHttpRequest");
                    return headers;
                }
            };

            DocHajj.getInstance().addToRequestQueue(jsonObjectRequest);

        } else {
            displayMessage(getString(R.string.something_went_wrong_net));
        }


    }

    @Override
    public void onDatesOkay(String[] data) {
        if (data.length > 0) {
            scheduleDate.setText(String.format(getString(R.string.no_days_selected), data.length));
            scheduledDate = data.toString();
            mSelectedDates = data;

            String theDaysSelected = "";
            for (int i = 0; i < mSelectedDates.length; i++) {
                try {
                    theDaysSelected = theDaysSelected + getDate(mSelectedDates[i]) + " " + getMonth(mSelectedDates[i]) + " ,";

                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            shownDateAll = theDaysSelected;
            if (scheduledTime != "")
                txtReqInfo.setText(shownDateAll + " at " + scheduledTime);
            else txtReqInfo.setText(shownDateAll);
            txtReqInfo.setVisibility(View.VISIBLE);

//            if (scheduledTime != "" && mTimePicker != null && mTimePicker.isShowing() == false)
//                mTimePicker.show();
        } else {
            scheduleDate.setText(R.string.chose_a_date);
        }
    }

    @Override
    public void onDatesCancel() {

    }

    class OnClick implements OnClickListener {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.frmDestination:
                    value = 0;
                    Intent intent2 = new Intent(getActivity(), CustomGooglePlacesSearch.class);
                    intent2.putExtra("cursor", "destination");
//                    intent2.putExtra("s_address", source.getText().toString());
                    intent2.putExtra("d_address", destination.getText().toString());
                    startActivityForResult(intent2, PLACE_AUTOCOMPLETE_REQUEST_CODE_DEST);
                    break;
                case R.id.lblPaymentChange:
                    if (cardInfoArrayList.size() > 0)
                        showChooser();
                    else
                        gotoAddCard();
                    break;
                case R.id.btnRequestRideConfirm:
//                    sendRequest();
                    break;
                case R.id.btnPayNow:
                    //                    payNow();
                    // internal payment is disabled
                    break;
                case R.id.btnSubmitReview:
                    submitReviewCall();
                    break;
                case R.id.btnCancelRide:
                    showCancelRideDialog();
                    break;
                case R.id.btnCancelTrip:
                    if (btnCancelTrip.getText().toString().equals(getString(R.string.cancel_trip)))
                        showCancelRideDialog();
                    else {
                        String shareUrl = URLHelper.REDIRECT_SHARE_URL;
                        navigateToShareScreen(shareUrl);
                    }
                    break;
                case R.id.imgSos:
                    showSosPopUp();
                    break;
                case R.id.imgShareRide:
                    String url = "http://maps.google.com/maps?q=loc:";
                    navigateToShareScreen(url);
                    break;
                case R.id.imgProvider:
                    Intent intent1 = new Intent(activity, ShowProfile.class);
                    intent1.putExtra("driver", driver);
                    startActivity(intent1);
                    break;
                case R.id.imgProviderRate:
                    Intent intent3 = new Intent(activity, ShowProfile.class);
                    intent3.putExtra("driver", driver);
                    startActivity(intent3);
                    break;
                case R.id.btnCall:

                    Log.d("CALLBTN", "sadsadsa");

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, 2);
                        Intent intentCall = new Intent(Intent.ACTION_CALL);
                        intentCall.setData(Uri.parse("tel:" + SharedHelper.getKey(context, "provider_mobile_no")));
                        startActivity(intentCall);
                    } else {
                        Intent intentCall = new Intent(Intent.ACTION_CALL);
                        intentCall.setData(Uri.parse("tel:" + SharedHelper.getKey(context, "provider_mobile_no")));
                        startActivity(intentCall);
                    }
                    break;
                case R.id.btnDone:
//                    pick_first = true;
//                    try {
//                        utils.print("centerLat", cmPosition.target.latitude + "");
//                        utils.print("centerLong", cmPosition.target.longitude + "");
//
//                        Geocoder geocoder = null;
//                        List<Address> addresses;
//                        geocoder = new Geocoder(getActivity(), Locale.getDefault());
//
//                        String city = "", state = "", address = "";
//
//                        try {
//                            addresses = geocoder.getFromLocation(cmPosition.target.latitude, cmPosition.target.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
//                            current_address = addresses.get(0).getAddressLine(0);
//                            city = addresses.get(0).getLocality();
//                            state = addresses.get(0).getAdminArea();
//                            // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
//                        } catch (Exception e) {
//                            final String surl = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + Double.toString(latitude) + "," + Double.toString(longitude) + "&sensor=true&key=" + context.getResources().getString(R.string.google_map_api);
//                            Log.d(TAG, "getAddress: " + surl);
//                            JSONObject object = new JSONObject();
//                            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, surl, object, new Response.Listener<JSONObject>() {
//                                @Override
//                                public void onResponse(JSONObject response) {
//                                    try {
//                                        JSONArray results = (JSONArray) response.get("results");
//                                        JSONObject resultsZero = results.getJSONObject(0);
//                                        String formatted_address = resultsZero.getString("formatted_address");
//                                        Log.d(TAG, "onResponse: " + formatted_address);
//                                        current_address = formatted_address;
//                                    } catch (JSONException e) {
//                                        e.printStackTrace();
//                                    }
//                                }
//
//                            }, new Response.ErrorListener() {
//                                @Override
//                                public void onErrorResponse(VolleyError error) {
//                                    current_address = "";
//                                }
//                            }) {
//                                @Override
//                                public Map<String, String> getHeaders() throws AuthFailureError {
//                                    HashMap<String, String> headers = new HashMap<String, String>();
//                                    return headers;
//                                }
//                            };
//
//                            DocHajj.getInstance().addToRequestQueue(jsonObjectRequest);
//                            e.printStackTrace();
//                        }
//
//
//                        serv_address = source_address = "" + current_address + "," + city + "," + state;
//                        serv_lat = source_lat = "" + cmPosition.target.latitude;
//                        serv_lng = source_lng = "" + cmPosition.target.longitude;
//
//                        mMap.clear();
//                        setValuesForSourceAndDestination();
//                        flowValue = 0;
//                        layoutChanges();
//                        strPickLocation = "";
//                        strPickType = "";
//
//                        CameraUpdate center = CameraUpdateFactory.newLatLng(new LatLng(cmPosition.target.latitude,
//                                cmPosition.target.longitude));
//                        CameraUpdate zoom = CameraUpdateFactory.zoomTo(15);
//                        mMap.moveCamera(center);
//                        mMap.moveCamera(zoom);
//
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                        Toast.makeText(context, "Can't able to get the address!.Please try again", Toast.LENGTH_SHORT).show();
//                    }
                    sendRequest();
                    break;
                case R.id.imgBack:
                    if (lnrRequestProviders.getVisibility() == View.VISIBLE) {
                        flowValue = 0;
                        if (!current_lat.equalsIgnoreCase("") && !current_lng.equalsIgnoreCase("")) {
                            LatLng myLocation = new LatLng(Double.parseDouble(current_lat), Double.parseDouble(current_lng));
                            CameraPosition cameraPosition = new CameraPosition.Builder().target(myLocation).zoom(14).build();
                            mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                        }

                    } else if (lnrApproximate.getVisibility() == View.VISIBLE) {
                        flowValue = 0;
                    } else if (lnrWaitingForProviders.getVisibility() == View.VISIBLE) {
                        flowValue = 2;
                    } else if (ScheduleLayout.getVisibility() == View.VISIBLE) {
                        flowValue = 2;
                    } else if (flowValue == 1) {
                        flowValue = 0;
                        destination.setText("");
                    }
                    layoutChanges();
                    break;
                case R.id.imgMenu:
                    if (NAV_DRAWER == 0) {
                        if (drawer != null)
                            drawer.openDrawer(Gravity.START);
                    } else {
                        NAV_DRAWER = 0;
                        drawer.closeDrawers();
                    }
                    break;
                case R.id.mapfocus:
                    Double crtLat, crtLng;
                    if (!current_lat.equalsIgnoreCase("") && !current_lng.equalsIgnoreCase("")) {
                        crtLat = Double.parseDouble(current_lat);
                        crtLng = Double.parseDouble(current_lng);

                        if (crtLat != null && crtLng != null) {
                            LatLng loc = new LatLng(crtLat, crtLng);
                            CameraPosition cameraPosition = new CameraPosition.Builder().target(loc).zoom(16).build();
                            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                        }
                    }
                    break;
                case R.id.imgSchedule:
                    flowValue = 7;
                    mSelectedDates = null;
                    cdd = new CustomDatePicker(activity, mFragment);
                    Calendar mcurrentTime = Calendar.getInstance();
                    int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                    int minute = mcurrentTime.get(Calendar.MINUTE);
                    mTimePicker = new TimePickerDialog(activity, new TimePickerDialog.OnTimeSetListener() {
                        int callCount = 0;   //To track number of calls to onTimeSet()

                        @Override
                        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

//                            if (callCount == 0) {
                            String choosedHour = "";
                            String choosedMinute = "";
                            String choosedTimeZone = "";
                            String choosedTime = "";

//                                scheduledTime = selectedHour + ":" + selectedMinute;

                            if (selectedHour > 12) {
                                choosedTimeZone = "PM";
                                selectedHour = selectedHour - 12;
                                if (selectedHour < 10) {
                                    choosedHour = "0" + selectedHour;
                                } else {
                                    choosedHour = "" + selectedHour;
                                }
                            } else {
                                choosedTimeZone = "AM";
                                if (selectedHour < 10) {
                                    choosedHour = "0" + selectedHour;
                                } else {
                                    choosedHour = "" + selectedHour;
                                }
                            }

                            if (selectedMinute < 10) {
                                choosedMinute = "0" + selectedMinute;
                            } else {
                                choosedMinute = "" + selectedMinute;
                            }
                            choosedTime = choosedHour + ":" + choosedMinute + " " + choosedTimeZone;

                            scheduledTime = choosedTime;

                            if (scheduledDate != "" && scheduledTime != "") {
                                Date date = null;
                                try {
                                    date = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).parse(cdd.allData()[0]);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                long milliseconds = date.getTime();
                                if (!DateUtils.isToday(milliseconds)) {
                                    scheduleTime.setText(choosedTime);
                                } else {
//                                        if (utils.checktimings(timeToCheck)) {
                                    scheduleTime.setText(choosedTime);
                                    mTimePicker.updateTime(selectedHour, selectedMinute);
//                                        } else {
//                                            Toast toast = new Toast(activity);
//                                            Toast.makeText(activity, getString(R.string.different_time), Toast.LENGTH_SHORT).show();
//                                        }
                                }
                            } else {
                                Toast.makeText(activity, getString(R.string.choose_date_time), Toast.LENGTH_SHORT).show();
                            }
//                            }
                            callCount++;

                        }
                    }, hour, minute, false);

                    cdd.clearDates();

                    layoutChanges();
                    break;
                case R.id.scheduleBtn:


                    break;
                case R.id.scheduleDate:
                    // calender class's instance and get current date , month and year from calender
                    cdd.show();

                    break;
                case R.id.scheduleTime:
                    mTimePicker.show();
                    break;
                case R.id.btnDonePopup:

                    lnrHidePopup.setVisibility(View.GONE);
                    lnrProviderPopup.startAnimation(slide_up_top);
                    lnrProviderPopup.setVisibility(View.GONE);

                    txtReqInfo.setAnimation(slide_down);
                    txtReqInfo.setVisibility(View.GONE);

                    lnrRequestProviders.setAnimation(slide_up);
                    lnrRequestProviders.setVisibility(View.VISIBLE);
                    lnrRequestProvidersIN.setAnimation(slide_up);
                    lnrRequestProvidersIN.setVisibility(View.VISIBLE);

                    lnrSearchAnimation.startAnimation(slide_up_down);
                    lnrSearchAnimation.setVisibility(View.VISIBLE);

                    getApproximateFare();

                    String day1H, day2H, day3H, day4H;

                    day1H = getTheDay(0) + "\n" + getTheDate(0);
                    day2H = getTheDay(1) + "\n" + getTheDate(1);
                    day3H = getTheDay(2) + "\n" + getTheDate(2);
                    day4H = getTheDay(3) + "\n" + getTheDate(3);

                    btnday1.setText(day1H);
                    btnday2.setText(day2H);
                    btnday3.setText(day3H);
                    btnday4.setText(day4H);

                    btntime1.setText(getHour(1));
                    btntime2.setText(getHour(2));
                    btntime3.setText(getHour(3));
                    btntime4.setText(getHour(4));

                    flowValue = 1;
//                    layoutChanges();
                    click = 1;
                    break;
                case R.id.btnRequestRidesNow:
                    if (mSelectedDates != null && mSelectedDates.length > 0 && scheduledTime != "") {
                        if (haveSurge != "" && Float.valueOf(surgeValue) > 1) {
                            txtReqInfo.setVisibility(View.GONE);
                            lnrRequestProviders.setAnimation(slide_down);
                            lnrRequestProviders.setVisibility(View.GONE);
                            lnrEstimatedV.setAnimation(slide_up);
                            lnrEstimatedV.setVisibility(View.VISIBLE);
                            surgeAdded.setText(surgeValue);
                            int estWSurge = (int) (startPrice * Float.valueOf(surgeValue));
                            lblSurgeEstPrice.setText(SharedHelper.getKey(context, "currency") + " " + String.valueOf(estWSurge) + "-" + String.valueOf(estWSurge + 100));
                        } else {
                            sendRequest();
                        }
                    } else {
                        sendRequest();
                    }
                    break;
                case R.id.btnRequestRides:
                    btnRequestRides.setVisibility(View.GONE);
                    linSchedule.setVisibility(View.VISIBLE);
                    break;
                case R.id.btnCancelEst:
                    lnrEstimatedV.setAnimation(slide_down);
                    lnrEstimatedV.setVisibility(View.GONE);
                    lnrRequestProviders.setAnimation(slide_up);
                    lnrRequestProviders.setVisibility(View.VISIBLE);
                    break;
                case R.id.btnAcceptEst:
                    sendRequest();
                    break;
                case R.id.btnday1:
                    mSelectedDates = new String[1];
                    if (btnday1.getCurrentTextColor() != getResources().getColor(R.color.text_color_white)) {

                        btnday1.setBackgroundResource(R.drawable.buttoncolorshape);
                        btnday1.setTextColor(getResources().getColor(R.color.text_color_white));
                        mSelectedDates[0] = getTheYear() + "-" + getTheMonth() + "-" + getTheDay(0);
                        String showndate1 = "";
                        try {
                            showndate1 = getMonth(mSelectedDates[0]) + " "
                                    + getDate(mSelectedDates[0]) + ", "
                                    + getYear(mSelectedDates[0]);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        shownDateAll = showndate1;
                        if (scheduledTime != "") {
                            txtReqInfo.setText(showndate1 + " at " + scheduledTime);
                        } else {
                            txtReqInfo.setText(showndate1);
                        }
                        txtReqInfo.setVisibility(View.VISIBLE);

                        btnday2.setBackgroundResource(R.drawable.buttonshape);
                        btnday2.setTextColor(getResources().getColor(R.color.black_text_color));
                        btnday3.setBackgroundResource(R.drawable.buttonshape);
                        btnday3.setTextColor(getResources().getColor(R.color.black_text_color));
                        btnday4.setBackgroundResource(R.drawable.buttonshape);
                        btnday4.setTextColor(getResources().getColor(R.color.black_text_color));
                    } else {
                        mSelectedDates[0] = "";
                        txtReqInfo.setText(scheduledDate);
                        txtReqInfo.setVisibility(View.GONE);

                        btnday1.setBackgroundResource(R.drawable.buttonshape);
                        btnday1.setTextColor(getResources().getColor(R.color.black_text_color));
                    }
                    break;
                case R.id.btnday2:
                    mSelectedDates = new String[1];
                    if (btnday2.getCurrentTextColor() != getResources().getColor(R.color.text_color_white)) {

                        btnday2.setBackgroundResource(R.drawable.buttoncolorshape);
                        btnday2.setTextColor(getResources().getColor(R.color.text_color_white));
                        mSelectedDates[0] = getTheYear() + "-" + getTheMonth() + "-" + getTheDay(1);
                        String showndate2 = "";
                        try {
                            showndate2 = getMonth(mSelectedDates[0]) + " "
                                    + getDate(mSelectedDates[0]) + ", "
                                    + getYear(mSelectedDates[0]);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        shownDateAll = showndate2;
                        if (scheduledTime != "") {
                            txtReqInfo.setText(showndate2 + " at " + scheduledTime);
                        } else {
                            txtReqInfo.setText(showndate2);
                        }
                        txtReqInfo.setVisibility(View.VISIBLE);

                        btnday1.setBackgroundResource(R.drawable.buttonshape);
                        btnday1.setTextColor(getResources().getColor(R.color.black_text_color));
                        btnday3.setBackgroundResource(R.drawable.buttonshape);
                        btnday3.setTextColor(getResources().getColor(R.color.black_text_color));
                        btnday4.setBackgroundResource(R.drawable.buttonshape);
                        btnday4.setTextColor(getResources().getColor(R.color.black_text_color));
                    } else {
                        mSelectedDates[0] = "";
                        txtReqInfo.setText(scheduledDate);
                        txtReqInfo.setVisibility(View.GONE);

                        btnday2.setBackgroundResource(R.drawable.buttonshape);
                        btnday2.setTextColor(getResources().getColor(R.color.black_text_color));
                    }
                    break;
                case R.id.btnday3:
                    mSelectedDates = new String[1];
                    if (btnday3.getCurrentTextColor() != getResources().getColor(R.color.text_color_white)) {

                        btnday3.setBackgroundResource(R.drawable.buttoncolorshape);
                        btnday3.setTextColor(getResources().getColor(R.color.text_color_white));

                        mSelectedDates[0] = getTheYear() + "-" + getTheMonth() + "-" + getTheDay(2);
                        String showndate3 = "";
                        try {
                            showndate3 = getMonth(mSelectedDates[0]) + " "
                                    + getDate(mSelectedDates[0]) + ", "
                                    + getYear(mSelectedDates[0]);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        shownDateAll = showndate3;
                        if (scheduledTime != "") {
                            txtReqInfo.setText(showndate3 + " at " + scheduledTime);
                        } else {
                            txtReqInfo.setText(showndate3);
                        }
                        txtReqInfo.setVisibility(View.VISIBLE);

                        btnday2.setBackgroundResource(R.drawable.buttonshape);
                        btnday2.setTextColor(getResources().getColor(R.color.black_text_color));
                        btnday1.setBackgroundResource(R.drawable.buttonshape);
                        btnday1.setTextColor(getResources().getColor(R.color.black_text_color));
                        btnday4.setBackgroundResource(R.drawable.buttonshape);
                        btnday4.setTextColor(getResources().getColor(R.color.black_text_color));
                    } else {
                        mSelectedDates[0] = "";
                        txtReqInfo.setText(scheduledDate);
                        txtReqInfo.setVisibility(View.GONE);

                        btnday3.setBackgroundResource(R.drawable.buttonshape);
                        btnday3.setTextColor(getResources().getColor(R.color.black_text_color));
                    }
                    break;
                case R.id.btnday4:
                    mSelectedDates = new String[1];
                    if (btnday4.getCurrentTextColor() != getResources().getColor(R.color.text_color_white)) {

                        btnday4.setBackgroundResource(R.drawable.buttoncolorshape);
                        btnday4.setTextColor(getResources().getColor(R.color.text_color_white));
                        mSelectedDates[0] = getTheYear() + "-" + getTheMonth() + "-" + getTheDay(3);
                        String showndate4 = "";
                        try {
                            showndate4 = getMonth(mSelectedDates[0]) + " "
                                    + getDate(mSelectedDates[0]) + ", "
                                    + getYear(mSelectedDates[0]);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        shownDateAll = showndate4;
                        if (scheduledTime != "") {
                            txtReqInfo.setText(showndate4 + " at " + scheduledTime);
                        } else {
                            txtReqInfo.setText(showndate4);
                        }
                        txtReqInfo.setVisibility(View.VISIBLE);

                        btnday1.setBackgroundResource(R.drawable.buttonshape);
                        btnday1.setTextColor(getResources().getColor(R.color.black_text_color));
                        btnday2.setBackgroundResource(R.drawable.buttonshape);
                        btnday2.setTextColor(getResources().getColor(R.color.black_text_color));
                        btnday3.setBackgroundResource(R.drawable.buttonshape);
                        btnday3.setTextColor(getResources().getColor(R.color.black_text_color));
                    } else {
                        mSelectedDates[0] = "";
                        txtReqInfo.setText(scheduledDate);
                        txtReqInfo.setVisibility(View.GONE);

                        btnday4.setBackgroundResource(R.drawable.buttonshape);
                        btnday4.setTextColor(getResources().getColor(R.color.black_text_color));
                    }
                    break;
                case R.id.btndayelse:
                    btnday1.setBackgroundResource(R.drawable.buttonshape);
                    btnday1.setTextColor(getResources().getColor(R.color.black_text_color));
                    btnday2.setBackgroundResource(R.drawable.buttonshape);
                    btnday2.setTextColor(getResources().getColor(R.color.black_text_color));
                    btnday3.setBackgroundResource(R.drawable.buttonshape);
                    btnday3.setTextColor(getResources().getColor(R.color.black_text_color));
                    btnday4.setBackgroundResource(R.drawable.buttonshape);
                    btnday4.setTextColor(getResources().getColor(R.color.black_text_color));
                    mSelectedDates = null;
                    txtReqInfo.setText("");
                    txtReqInfo.setVisibility(View.GONE);


                    mSelectedDates = null;
                    cdd = new CustomDatePicker(activity, mFragment);
                    cdd.clearDates();
                    cdd.show();

                    break;
                case R.id.btntime1:
                    if (btntime1.getCurrentTextColor() != getResources().getColor(R.color.text_color_white)) {

                        btntime1.setBackgroundResource(R.drawable.buttoncolorshape);
                        btntime1.setTextColor(getResources().getColor(R.color.text_color_white));

                        scheduledTime = btntime1.getText().toString().replace("\n", ":00 ");
                        if (mSelectedDates != null && shownDateAll != "")
                            txtReqInfo.setText(shownDateAll + " at " + scheduledTime);
                        else txtReqInfo.setText(scheduledTime);
                        txtReqInfo.setVisibility(View.VISIBLE);

                        btntime2.setBackgroundResource(R.drawable.buttonshape);
                        btntime2.setTextColor(getResources().getColor(R.color.black_text_color));
                        btntime3.setBackgroundResource(R.drawable.buttonshape);
                        btntime3.setTextColor(getResources().getColor(R.color.black_text_color));
                        btntime4.setBackgroundResource(R.drawable.buttonshape);
                        btntime4.setTextColor(getResources().getColor(R.color.black_text_color));
                    } else {
                        scheduledTime = "";
                        txtReqInfo.setText(scheduledTime);
                        txtReqInfo.setVisibility(View.GONE);

                        btntime1.setBackgroundResource(R.drawable.buttonshape);
                        btntime1.setTextColor(getResources().getColor(R.color.black_text_color));
                    }
                    break;
                case R.id.btntime2:
                    if (btntime2.getCurrentTextColor() != getResources().getColor(R.color.text_color_white)) {
                        btntime2.setBackgroundResource(R.drawable.buttoncolorshape);
                        btntime2.setTextColor(getResources().getColor(R.color.text_color_white));

                        scheduledTime = btntime2.getText().toString().replace("\n", ":00 ");
                        if (mSelectedDates != null && shownDateAll != "")
                            txtReqInfo.setText(shownDateAll + " at " + scheduledTime);
                        else txtReqInfo.setText(scheduledTime);
                        txtReqInfo.setVisibility(View.VISIBLE);

                        btntime1.setBackgroundResource(R.drawable.buttonshape);
                        btntime1.setTextColor(getResources().getColor(R.color.black_text_color));
                        btntime3.setBackgroundResource(R.drawable.buttonshape);
                        btntime3.setTextColor(getResources().getColor(R.color.black_text_color));
                        btntime4.setBackgroundResource(R.drawable.buttonshape);
                        btntime4.setTextColor(getResources().getColor(R.color.black_text_color));
                    } else {
                        scheduledTime = "";
                        txtReqInfo.setText(scheduledTime);
                        txtReqInfo.setVisibility(View.GONE);

                        btntime2.setBackgroundResource(R.drawable.buttonshape);
                        btntime2.setTextColor(getResources().getColor(R.color.black_text_color));
                    }
                    break;
                case R.id.btntime3:
                    if (btntime3.getCurrentTextColor() != getResources().getColor(R.color.text_color_white)) {
                        btntime3.setBackgroundResource(R.drawable.buttoncolorshape);
                        btntime3.setTextColor(getResources().getColor(R.color.text_color_white));

                        scheduledTime = btntime3.getText().toString().replace("\n", ":00 ");
                        if (mSelectedDates != null && shownDateAll != "")
                            txtReqInfo.setText(shownDateAll + " at " + scheduledTime);
                        else txtReqInfo.setText(scheduledTime);
                        txtReqInfo.setVisibility(View.VISIBLE);

                        btntime1.setBackgroundResource(R.drawable.buttonshape);
                        btntime1.setTextColor(getResources().getColor(R.color.black_text_color));
                        btntime2.setBackgroundResource(R.drawable.buttonshape);
                        btntime2.setTextColor(getResources().getColor(R.color.black_text_color));
                        btntime4.setBackgroundResource(R.drawable.buttonshape);
                        btntime4.setTextColor(getResources().getColor(R.color.black_text_color));
                    } else {
                        scheduledTime = "";
                        txtReqInfo.setText(scheduledTime);
                        txtReqInfo.setVisibility(View.GONE);

                        btntime3.setBackgroundResource(R.drawable.buttonshape);
                        btntime3.setTextColor(getResources().getColor(R.color.black_text_color));
                    }
                    break;
                case R.id.btntime4:
                    if (btntime4.getCurrentTextColor() != getResources().getColor(R.color.text_color_white)) {
                        btntime4.setBackgroundResource(R.drawable.buttoncolorshape);
                        btntime4.setTextColor(getResources().getColor(R.color.text_color_white));

                        scheduledTime = btntime4.getText().toString().replace("\n", ":00 ");
                        if (mSelectedDates != null && shownDateAll != "")
                            txtReqInfo.setText(shownDateAll + " at " + scheduledTime);
                        else txtReqInfo.setText(scheduledTime);
                        txtReqInfo.setVisibility(View.VISIBLE);

                        btntime1.setBackgroundResource(R.drawable.buttonshape);
                        btntime1.setTextColor(getResources().getColor(R.color.black_text_color));
                        btntime2.setBackgroundResource(R.drawable.buttonshape);
                        btntime2.setTextColor(getResources().getColor(R.color.black_text_color));
                        btntime3.setBackgroundResource(R.drawable.buttonshape);
                        btntime3.setTextColor(getResources().getColor(R.color.black_text_color));
                    } else {
                        scheduledTime = "";
                        txtReqInfo.setText(scheduledTime);
                        txtReqInfo.setVisibility(View.GONE);

                        btntime4.setBackgroundResource(R.drawable.buttonshape);
                        btntime4.setTextColor(getResources().getColor(R.color.black_text_color));
                    }
                    break;
                case R.id.btntimeelse:
                    btntime1.setBackgroundResource(R.drawable.buttonshape);
                    btntime1.setTextColor(getResources().getColor(R.color.black_text_color));
                    btntime2.setBackgroundResource(R.drawable.buttonshape);
                    btntime2.setTextColor(getResources().getColor(R.color.black_text_color));
                    btntime3.setBackgroundResource(R.drawable.buttonshape);
                    btntime3.setTextColor(getResources().getColor(R.color.black_text_color));
                    btntime4.setBackgroundResource(R.drawable.buttonshape);
                    btntime4.setTextColor(getResources().getColor(R.color.black_text_color));
                    scheduledTime = "";
                    txtReqInfo.setText(scheduledTime);
                    txtReqInfo.setVisibility(View.GONE);

                    Calendar currentTime = Calendar.getInstance();
                    int nowhour = currentTime.get(Calendar.HOUR_OF_DAY);
                    int nowminute = currentTime.get(Calendar.MINUTE);
                    mTimePicker = new TimePickerDialog(activity, new TimePickerDialog.OnTimeSetListener() {
                        int callCount = 0;   //To track number of calls to onTimeSet()

                        @Override
                        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

//                            if (callCount == 0) {
                            String choosedHour = "";
                            String choosedMinute = "";
                            String choosedTimeZone = "";
                            String choosedTime = "";

//                                scheduledTime = selectedHour + ":" + selectedMinute;

                            if (selectedHour >= 12) {
                                choosedTimeZone = "PM";
                                selectedHour = selectedHour - 12;
                                if (selectedHour < 10) {
                                    choosedHour = "0" + selectedHour;
                                } else {
                                    choosedHour = "" + selectedHour;
                                }
                            } else {
                                choosedTimeZone = "AM";
                                if (selectedHour < 10) {
                                    choosedHour = "0" + selectedHour;
                                } else {
                                    choosedHour = "" + selectedHour;
                                }
                            }

                            if (selectedMinute < 10) {
                                choosedMinute = "0" + selectedMinute;
                            } else {
                                choosedMinute = "" + selectedMinute;
                            }
                            choosedTime = choosedHour + ":" + choosedMinute + " " + choosedTimeZone;


                            if (mSelectedDates != null) {

                                scheduledTime = choosedTime;
                                if (shownDateAll != "")
                                    txtReqInfo.setText(shownDateAll + " at " + scheduledTime);
                                else txtReqInfo.setText(scheduledTime);
                                txtReqInfo.setVisibility(View.VISIBLE);
//                                Date date = null;
//                                try {
//                                    date = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).parse(cdd.allData()[0]);
//                                } catch (ParseException e) {
//                                    e.printStackTrace();
//                                }
//                                long milliseconds = date.getTime();
//                                if (!DateUtils.isToday(milliseconds)) {
//////                                    scheduleTime.setText(choosedTime);
//                                } else {
////                                    if (utils.checktimings(timeToCheck)) {
//////                                    scheduleTime.setText(choosedTime);
//                                    mTimePicker.updateTime(selectedHour, selectedMinute);
////                                    } else {
////                                        Toast toast = new Toast(activity);
////                                        Toast.makeText(activity, getString(R.string.different_time), Toast.LENGTH_SHORT).show();
////                                    }
//                                }
                            } else {
                                Toast.makeText(activity, getString(R.string.choose_date_time), Toast.LENGTH_SHORT).show();
                            }
                            callCount++;
                        }
                    }, nowhour, nowminute, false);
                    mTimePicker.show();
                    break;
                case R.id.addPromo:
                    Intent i = new Intent(getActivity(), CouponActivity.class);
                    startActivityForResult(i, PROMO_CODE_REQUEST);
            }
        }

    }


    public String getHour(int h) {
        Calendar cal = Calendar.getInstance();

        cal.add(Calendar.HOUR, h);

        String hour = new SimpleDateFormat("hh", Locale.ENGLISH).format(cal.getTime());
        String theTime = new SimpleDateFormat("a", Locale.ENGLISH).format(cal.getTime());
//        int gHour = Integer.parseInt(hour);

        return hour + "\n" + theTime.toUpperCase();
    }

    public String getTheDay(int day) {
        Calendar calendar = Calendar.getInstance();

        calendar.add(Calendar.DAY_OF_MONTH, day);

//        DateFormat dateFormat = new SimpleDateFormat("dd");

        String thedayAsString = new SimpleDateFormat("dd", Locale.ENGLISH).format(calendar.getTime());

        return thedayAsString;
    }

    public String getTheMonth() {
        Calendar calendar = Calendar.getInstance();

        DateFormat dateFormat = new SimpleDateFormat("MM", Locale.ENGLISH);

        Date theday = calendar.getTime();

        String thedayAsString = dateFormat.format(theday);

        return thedayAsString;
    }

    public String getTheYear() {
        Calendar calendar = Calendar.getInstance();

        DateFormat dateFormat = new SimpleDateFormat("yyyy", Locale.ENGLISH);

        Date theday = calendar.getTime();

        String thedayAsString = dateFormat.format(theday);

        return thedayAsString;
    }

    public String getTheDate(int day) {
        Calendar calendar = Calendar.getInstance();

        calendar.add(Calendar.DAY_OF_WEEK, day);

        Date theday = calendar.getTime();

        String thedayCar = String.valueOf(theday);

        return thedayCar.substring(0, Math.min(thedayCar.length(), 3)).toUpperCase();
    }


    public String getMonth(String date) throws ParseException {
        Date d = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(date);
        Calendar cal = Calendar.getInstance();
        cal.setTime(d);
        String monthName = new SimpleDateFormat("MMM", Locale.ENGLISH).format(cal.getTime());
        return monthName;
    }

    public String getDate(String date) throws ParseException {
        Date d = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(date);
        Calendar cal = Calendar.getInstance();
        cal.setTime(d);
        String dateName = new SimpleDateFormat("dd", Locale.ENGLISH).format(cal.getTime());
        return dateName;
    }

    public String getYear(String date) throws ParseException {
        Date d = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(date);
        Calendar cal = Calendar.getInstance();
        cal.setTime(d);
        String yearName = new SimpleDateFormat("yyyy", Locale.ENGLISH).format(cal.getTime());
        return yearName;
    }


    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }

        private String downloadUrl(String strUrl) throws IOException {
            String data = "";
            InputStream iStream = null;
            HttpURLConnection urlConnection = null;
            try {
                utils.print("Entering dowload url", "entrng");
                URL url = new URL(strUrl);

                // Creating an http connection to communicate with url
                urlConnection = (HttpURLConnection) url.openConnection();

                // Connecting to url
                urlConnection.connect();

                // Reading data from url
                iStream = urlConnection.getInputStream();

                BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

                StringBuffer sb = new StringBuffer();

                String line = "";
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }

                data = sb.toString();

                br.close();

            } catch (Exception e) {

            } finally {
                iStream.close();
                urlConnection.disconnect();
            }
            return data;
        }

        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service

            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
                utils.print("Entering dwnload task", "download task");
            } catch (Exception e) {
                utils.print("Background Task", e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            utils.print("Resultmap", result);

            parserTask = new ParserTask();
            parserTask.execute(result);
        }
    }

    /*
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, PolylineOptions> {
        //ProgressDialog progressDialog = new ProgressDialog(activity);
        // Parsing the data in non-ui thread
        PolylineOptions lineOptions = null;
        boolean polyDrawn = true;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            points.clear();
        }

        @Override
        protected PolylineOptions doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);

                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
                utils.print("routes", routes.toString());
                utils.print("routes size", routes.size() + "");
                ///////////////////////////////////
                if (routes != null) {
                    // Traversing through all the routes
                    for (int i = 0; i < routes.size(); i++) {
                        lineOptions = new PolylineOptions();

                        // Fetching i-th route
                        List<HashMap<String, String>> path = routes.get(i);

                        // Fetching all the points in i-th route
                        utils.print("path size", path.size() + "");
                        for (int j = 0; j < path.size(); j++) {
                            HashMap<String, String> point = path.get(j);
                            if (!point.get("lat").equalsIgnoreCase("") && !point.get("lng").equalsIgnoreCase("")) {
                                double lat = Double.parseDouble(point.get("lat"));
                                double lng = Double.parseDouble(point.get("lng"));
                                LatLng position = new LatLng(lat, lng);
                                points.add(position);
                                utils.print("abcde", points.toString());

                                if (j == 0) {
                                    sourceLatLng = new LatLng(lat, lng);
                                }
                                if (j == path.size()) {
                                    destLatLng = new LatLng(lat, lng);
                                }
                            }
                        }

                        // Adding all the points in the route to LineOptions
                        lineOptions.addAll(points);
                        lineOptions.width(5);
                        lineOptions.color(Color.BLACK);

                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return lineOptions;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(PolylineOptions lineOptions) {
            MarkerOptions markerOptions = new MarkerOptions()
                    .position(sourceLatLng).title("Service Location").draggable(true)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.mypos));

            sourceMarker = mMap.addMarker(markerOptions);
            sourceMarker.setDraggable(true);

            if (lineOptions == null) {
                flowValue = 0;
                layoutChanges();
                Intent intent2 = new Intent(getActivity(), CustomGooglePlacesSearch.class);
                intent2.putExtra("cursor", "destination");
//                intent2.putExtra("s_address", source.getText().toString());
                startActivityForResult(intent2, PLACE_AUTOCOMPLETE_REQUEST_CODE_DEST);
            } else {
                if (customDialog != null) {
                    customDialog.dismiss();
                }
            }
        }
    }


    private class ServiceListAdapter extends RecyclerView.Adapter<ServiceListAdapter.MyViewHolder> {
        JSONArray jsonArray;
        int selectedPosition = -1;
        private SparseBooleanArray selectedItems;

        public ServiceListAdapter(JSONArray array) {
            this.jsonArray = array;
            currentPostion = -1;
        }


        @Override
        public ServiceListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            @SuppressLint("InflateParams") View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.service_type_list_item, parent, false);
            return new ServiceListAdapter.MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ServiceListAdapter.MyViewHolder holder, final int position) {
            utils.print("Title: ", "pos:" + currentPostion + " " + jsonArray.optJSONObject(position).optString("name") + " Image: " + jsonArray.optJSONObject(position).optString("image") + " Grey_Image:" + jsonArray.optJSONObject(position).optString("grey_image"));

            holder.serviceTitle.setText(jsonArray.optJSONObject(position).optString("name"));
            if (position == currentPostion) {
                SharedHelper.putKey(context, "service_type", "" + jsonArray.optJSONObject(position).optString("id"));
                Glide.with(activity)
                        .load(jsonArray.optJSONObject(position).optString("image"))
                        .placeholder(R.drawable.doctor_male)
                        .dontAnimate()
                        .error(R.drawable.doctor_male)
                        .into(holder.serviceImg);

            } else {
                Glide.with(activity)
                        .load(jsonArray.optJSONObject(position).optString("image"))
                        .placeholder(R.drawable.doctor_male)
                        .dontAnimate()
                        .error(R.drawable.doctor_male)
                        .into(holder.serviceImg);
            }

            holder.thisview.setTag(position);

            holder.thisview.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {

                    HomeFragment.this.currentPostion = Integer.parseInt(view.getTag().toString());

                    SharedHelper.putKey(HomeFragment.this.context, "service_type", "" + HomeFragment.ServiceListAdapter.this.jsonArray.optJSONObject(Integer.parseInt(view.getTag().toString())).optString("id"));
                    SharedHelper.putKey(HomeFragment.this.context, "name", "" + HomeFragment.ServiceListAdapter.this.jsonArray.optJSONObject(HomeFragment.this.currentPostion).optString("name"));
                    HomeFragment.ServiceListAdapter.this.notifyDataSetChanged();
                    HomeFragment.this.utils.print("service_type", "" + SharedHelper.getKey(HomeFragment.this.context, "service_type"));
                    HomeFragment.this.utils.print("Service name", "" + SharedHelper.getKey(HomeFragment.this.context, "name"));
                    HomeFragment.this.getProvidersList(SharedHelper.getKey(HomeFragment.this.context, "service_type"));
                    try {
                        HomeFragment.this.showProviderPopup(HomeFragment.ServiceListAdapter.this.jsonArray.getJSONObject(position));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

        }

        @Override
        public int getItemCount() {
            return this.jsonArray.length();
        }

        public class MyViewHolder extends ViewHolder {

            View thisview;
            MyTextView serviceTitle;
            ImageView serviceImg;
            LinearLayout linearLayoutOfList;

            public MyViewHolder(View itemView) {
                super(itemView);
                this.serviceTitle = itemView.findViewById(R.id.serviceItem);
                this.serviceImg = itemView.findViewById(R.id.serviceImg);
                this.linearLayoutOfList = itemView.findViewById(R.id.LinearLayoutOfList);
                this.thisview = itemView;
//                this.selector_background = itemView.findViewById(R.id.selector_background);
//                HomeFragment.this.height = itemView.getHeight();
//                HomeFragment.this.width = itemView.getWidth();
            }
        }
    }


    EventsBroadCastReciver eventsBroadCastReciver;

    class EventsBroadCastReciver extends BroadcastReceiver {
        @Override
        public void onReceive(Context mContext, Intent mIntent) {
            checkStatus();
        }

    }


    @Override
    public void onStart() {
        super.onStart();
        eventsBroadCastReciver = new EventsBroadCastReciver();
        getContext().registerReceiver(eventsBroadCastReciver, new IntentFilter(REQUEST_STATUS_UPDATE));

    }


    @Override
    public void onResume() {
        super.onResume();
        checkStatus();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (eventsBroadCastReciver != null) {
            getContext().unregisterReceiver(eventsBroadCastReciver);
        }
    }
}