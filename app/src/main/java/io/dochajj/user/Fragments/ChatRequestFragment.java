package io.dochajj.user.Fragments;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.dochajj.user.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChatRequestFragment extends Fragment {


    @BindView(R.id.ivMenu)
    ImageView ivMenu;
    Unbinder unbinder;

    Activity activity;
    Context context;
    Fragment mFragment;


    DrawerLayout drawer;
    int NAV_DRAWER = 0;


    View rootView;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        this.mFragment = this;

    }

    public ChatRequestFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_chat_request, container, false);

        drawer = getActivity().findViewById(R.id.drawer_layout);

        unbinder = ButterKnife.bind(this, rootView);


        rootView.setFocusableInTouchMode(true);
        rootView.requestFocus();
        rootView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() != KeyEvent.ACTION_DOWN)
                    return true;

                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.frame, new HomeFragment(), "backToHome");
                    fragmentTransaction.commitAllowingStateLoss();
                    return true;
                }
                return false;
            }
        });


        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.ivMenu)
    public void onViewClicked() {
        if (NAV_DRAWER == 0) {
            if (drawer != null)
                drawer.openDrawer(Gravity.START);
        } else {
            NAV_DRAWER = 0;
            drawer.closeDrawers();
        }
    }
}
