package io.dochajj.user.Utils.UI;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.squareup.timessquare.CalendarPickerView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import io.dochajj.user.R;

/**
 * Created by supergenedy on 21-Feb-18.
 */

public class CustomDatePicker extends Dialog implements
        View.OnClickListener {

    private Activity c;
    private Fragment c2;
    public Dialog d;
    public Button yes, no;
    public String[] scheduledDate;
    static private Collection<Date> mSelectedDates;
    CalendarPickerView calendar_view;


    // callback handler
    public interface  OnCompleteHandler{
        void onDatesOkay(String[] data);
        void onDatesCancel();
    }

    OnCompleteHandler mHandler;


    public CustomDatePicker(Activity a , Fragment f) {
        super(a);
        this.c = a;
        this.c2 = f;
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        try {
        mHandler = (OnCompleteHandler) this.c2;
        } catch (ClassCastException e) {
            throw new ClassCastException(" must implement OnItemClickedListener");
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_date_picker);
        yes = findViewById(R.id.btn_yes);
        no = findViewById(R.id.btn_no);
        yes.setOnClickListener(this);
        no.setOnClickListener(this);
        calendar_view = findViewById(R.id.calendar_view);

        Calendar thisMoth = Calendar.getInstance();
        thisMoth.add(Calendar.WEEK_OF_MONTH, 2);


        Date today = new Date();

        //add one week to calendar from todays date
        calendar_view.init(today, thisMoth.getTime()).inMode(CalendarPickerView.SelectionMode.MULTIPLE);


        if (mSelectedDates != null)
            for (Date date:mSelectedDates) {
                calendar_view.selectDate(date);
            }

        calendar_view.setOnDateSelectedListener(new CalendarPickerView.OnDateSelectedListener() {
            @Override
            public void onDateSelected(Date date) {

                SimpleDateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy");

                String stringDate = outputFormat.format(date);

//                Toast.makeText(getContext(), "Selected Date is : " + stringDate, Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onDateUnselected(Date date) {

                SimpleDateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy");

                String stringDate = outputFormat.format(date);

//                Toast.makeText(getContext(), "UnSelected Date is : " + stringDate, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_yes:

                mSelectedDates = calendar_view.getSelectedDates();

                String allDates[] = new String[mSelectedDates.size()];
                SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd");

                for (int i = 0; i < calendar_view.getSelectedDates().size(); i++) {
                    //here you can fetch all dates
                    Date thatDate = calendar_view.getSelectedDates().get(i);

                    String selectedDate = outputFormat.format(thatDate);

                    allDates[i] = selectedDate;
                }
//                for (int i = 0; i < allDates.length; i++) {
//                    String strAllDates = "Date[" + i + "] = " + allDates[i];
//                    Toast.makeText(getContext(), strAllDates, Toast.LENGTH_SHORT).show();
//                }

                scheduledDate = allDates;

//                Toast.makeText(getContext(), scheduledDate, Toast.LENGTH_SHORT).show();
                allData();


                this.mHandler.onDatesOkay(allDates);


                dismiss();

                break;
            case R.id.btn_no:

                this.mHandler.onDatesCancel();

                dismiss();
                break;
            default:
                break;
        }
        dismiss();
    }

    public void clearDates() {
        mSelectedDates = null;
    }

    public String[] allData(){

        return scheduledDate;
    }
}
