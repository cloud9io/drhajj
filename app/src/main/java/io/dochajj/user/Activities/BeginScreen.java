package io.dochajj.user.Activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import io.dochajj.user.R;


public class BeginScreen extends AppCompatActivity {

    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 123;
    TextView enter_ur_mailID;
    LinearLayout social_layout;
    ImageView socialLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Mint.setApplicationEnvironment(Mint.appEnvironmentStaging);
//        Mint.initAndStartSession(this.getApplication(), "554ece22");
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_begin);

        checkAndRequestPermissions();

//        enter_ur_mailID = (TextView)findViewById(R.id.enter_ur_mailID);
//        social_layout = (LinearLayout) findViewById(R.id.social_layout);

        Button signIn = (Button) findViewById(R.id.buttonSign);
        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mainIntent = new Intent(BeginScreen.this, ActivityPassword.class);
                //   mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(mainIntent);
                overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                // BeginScreen.this.finish();
            }
        });

        Button register = (Button) findViewById(R.id.buttonRegister);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mainIntent = new Intent(BeginScreen.this, RegisterActivity.class);
                startActivity(mainIntent);
                overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
            }
        });
//        socialLogin = findViewById(R.id.socialLogin);
//        socialLogin.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent mainIntent = new Intent(BeginScreen.this, ActivitySocialLogin.class);
//                startActivity(mainIntent);
//                overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
//            }
//        });

    }


    private boolean checkAndRequestPermissions() {
//        int camera = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA);
//        int storage = ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int loc = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
        int loc2 = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
//        int callPhone = ContextCompat.checkSelfPermission(this , android.Manifest.permission.CALL_PHONE);
        int wifiState = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_WIFI_STATE);
        int changeWifi = ContextCompat.checkSelfPermission(this, Manifest.permission.CHANGE_WIFI_MULTICAST_STATE);
//        int getAccounts = ContextCompat.checkSelfPermission(this , android.Manifest.permission.GET_ACCOUNTS);
//        int readCallLog = ContextCompat.checkSelfPermission(this , android.Manifest.permission.READ_CALL_LOG);
//        int readState = ContextCompat.checkSelfPermission(this , android.Manifest.permission.READ_PHONE_STATE);

        // sinch sms
//        int readCallLog2 = ContextCompat.checkSelfPermission(this , android.Manifest.permission.READ_SMS);
//        int readCallLog3 = ContextCompat.checkSelfPermission(this , android.Manifest.permission.RECEIVE_SMS);

//        int readExternal = ContextCompat.checkSelfPermission(this , android.Manifest.permission.READ_EXTERNAL_STORAGE);

        List<String> listPermissionsNeeded = new ArrayList<>();

        if (loc2 != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (loc != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        }
        if (wifiState != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_WIFI_STATE);
        }
        if (changeWifi != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CHANGE_WIFI_MULTICAST_STATE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray
                    (new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }


//    private void setLocale(String lang) {
//        Locale locale = new Locale(lang);
//        Locale.setDefault(locale);
//        Configuration config = new Configuration();
//        config.locale = locale;
//        getBaseContext().getResources().updateConfiguration(config,getBaseContext().getResources().getDisplayMetrics());
//        SharedHelper.putKey(getApplicationContext(),"language",lang);
//    }
//
//    public void loadLocale(){
//        setLocale(SharedHelper.getKey(getApplicationContext(),"language"));
//    }


}
