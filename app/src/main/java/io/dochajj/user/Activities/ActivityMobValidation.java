package io.dochajj.user.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import io.dochajj.user.Helper.SharedHelper;
import io.dochajj.user.R;

/**
 * Created by Pcraft.org on 28/01/18.
 */

public class ActivityMobValidation extends AppCompatActivity {

    public static final String MY_FIELD = "TEXT_MOBILE_NUMBER";
    EditText text_mobile_number;
    Button button_submit;

    public static Activity fa;

    private String mPhoneNumber;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_mob_validation);

        this.text_mobile_number = this.findViewById(R.id.text_mobile_number);
        this.button_submit = this.findViewById(R.id.button_submit);
        fa = this;

        if (savedInstanceState != null) {
            String value = savedInstanceState.getString(MY_FIELD);
            text_mobile_number.setText(value);
        } else {
            mPhoneNumber = SharedHelper.getKey(getBaseContext(), "mobile");
            if (mPhoneNumber.length() > 9 && mPhoneNumber != null && mPhoneNumber != "null"){
                text_mobile_number.setText(mPhoneNumber);
            }
        }

        button_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityMobValidation.this.verifyNumberAndSubmit();
            }
        });
    }

    void verifyNumberAndSubmit() {
        mPhoneNumber = text_mobile_number.getText().toString();

        if (mPhoneNumber.equals("") == false && mPhoneNumber.startsWith("01") && mPhoneNumber.length() == 11){
            Intent i = new Intent(ActivityMobValidation.this , ActivityVerifyOtp.class);
            i.putExtra(MY_FIELD , text_mobile_number.getText().toString());
            startActivity(i);
        }else{
             Toast.makeText(getApplicationContext() , R.string.please_verify_phone , Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {

        mPhoneNumber = text_mobile_number.getText().toString();
        outState.putString(ActivityMobValidation.MY_FIELD, this.mPhoneNumber);
        super.onSaveInstanceState(outState);
    }


}
