package io.dochajj.user.Activities;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.android.volley.NetworkResponse;
import com.android.volley.Request.Method;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.braintreepayments.cardform.view.CardForm;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import io.dochajj.user.Helper.CustomDialog;
import io.dochajj.user.Helper.SharedHelper;
import io.dochajj.user.Helper.URLHelper;
import io.dochajj.user.DocHajj;
import io.dochajj.user.Utils.MyButton;
import io.dochajj.user.Utils.Utilities;
import io.dochajj.user.R;

import static io.dochajj.user.DocHajj.trimMessage;

public class AddCard extends AppCompatActivity {

    static final Pattern CODE_PATTERN = Pattern
            .compile("([0-9]{0,4})|([0-9]{4}-)+|([0-9]{4}-[0-9]{0,4})+");
    Activity activity;
    Context context;
    ImageView backArrow, help_month_and_year, help_cvv;
    MyButton addCard;
    //EditText cardNumber, cvv, month_and_year;
    CardForm cardForm;
    String Card_Token = "";
    CustomDialog customDialog;
    Utilities utils = new Utilities();
    Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_add_card);
        this.findViewByIdAndInitialize();

//        cardNumber.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                if (s.length() > 0 && !CODE_PATTERN.matcher(s).matches()) {
//                    String input = s.toString();
//                    String numbersOnly = keepNumbersOnly(input);
//                    String code = formatNumbersAsCode(numbersOnly);
//                    cardNumber.removeTextChangedListener(this);
//                    cardNumber.setText(code);
//                    cardNumber.setSelection(code.length());
//                    cardNumber.addTextChangedListener(this);
//                }
//            }
//
//            private String keepNumbersOnly(CharSequence s) {
//                return s.toString().replaceAll("[^0-9]", "");
//            }
//
//            private String formatNumbersAsCode(CharSequence s) {
//                int groupDigits = 0;
//                String tmp = "";
//                for (int i = 0; i < s.length(); ++i) {
//                    tmp += s.charAt(i);
//                    ++groupDigits;
//                    if (groupDigits == 4) {
//                        tmp += "-";
//                        groupDigits = 0;
//                    }
//                }
//                return tmp;
//            }
//        });
//
//
//        month_and_year.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//                if (s.length() > 0 && !CODE_PATTERN.matcher(s).matches()) {
//                    String input = s.toString();
//                    String numbersOnly = keepNumbersOnly(input);
//                    String code = formatNumbersAsCode(numbersOnly);
//                    cardNumber.removeTextChangedListener(this);
//                    cardNumber.setText(code);
//                    cardNumber.setSelection(code.length());
//                    cardNumber.addTextChangedListener(this);
//                }
//
//            }
//
//            private String keepNumbersOnly(CharSequence s) {
//                return s.toString().replaceAll("[^0-9]", "");
//            }
//
//            private String formatNumbersAsCode(CharSequence s) {
//                int groupDigits = 0;
//                String tmp = "";
//                for (int i = 0; i < s.length(); ++i) {
//                    tmp += s.charAt(i);
//                    ++groupDigits;
//                    if (groupDigits == 4) {
//                        tmp += "-";
//                        groupDigits = 0;
//                    }
//                }
//                return tmp;
//            }
//
//
//        });


        this.backArrow.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AddCard.this.onBackPressed();
            }
        });

        this.addCard.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {


                customDialog = new CustomDialog(AddCard.this);
                AddCard.this.customDialog.setCancelable(false);
                AddCard.this.customDialog.show();
                if (AddCard.this.cardForm.getCardNumber() == null || AddCard.this.cardForm.getExpirationMonth() == null || AddCard.this.cardForm.getExpirationYear() == null || AddCard.this.cardForm.getCvv() == null) {
                    AddCard.this.customDialog.dismiss();
                    AddCard.this.displayMessage(AddCard.this.getString(R.string.enter_card_details));
                } else {
                    if (AddCard.this.cardForm.getCardNumber().equals("") || AddCard.this.cardForm.getExpirationMonth().equals("") || AddCard.this.cardForm.getExpirationYear().equals("") || AddCard.this.cardForm.getCvv().equals("")) {
                        AddCard.this.customDialog.dismiss();
                        AddCard.this.displayMessage(AddCard.this.getString(R.string.enter_card_details));
                    } else {
                        String cardNumber = AddCard.this.cardForm.getCardNumber();
                        int month = Integer.parseInt(AddCard.this.cardForm.getExpirationMonth());
                        int year = Integer.parseInt(AddCard.this.cardForm.getExpirationYear());
                        String cvv = AddCard.this.cardForm.getCvv();

                        AddCard.this.addCardToAccount(AddCard.this.cardForm);
                    }

                }
            }
        });

    }


    public void findViewByIdAndInitialize() {
        this.backArrow = this.findViewById(R.id.backArrow);
//        help_month_and_year = (ImageView)findViewById(R.id.help_month_and_year);
//        help_cvv = (ImageView)findViewById(R.id.help_cvv);
        this.addCard = this.findViewById(R.id.addCard);
//        cardNumber = (EditText) findViewById(R.id.cardNumber);
//        cvv = (EditText) findViewById(R.id.cvv);
//        month_and_year = (EditText) findViewById(R.id.monthAndyear);
        this.context = this;
        this.activity = this;
        this.cardForm = this.findViewById(R.id.card_form);
        this.cardForm.cardRequired(true)
                .expirationRequired(true)
                .cvvRequired(true)
                .postalCodeRequired(false)
                .mobileNumberRequired(false)
                .actionLabel("Add Card")
                .setup(this.activity);


    }


    public static class LocalWebView extends WebView {
        public LocalWebView(Context context) {
            super(context);
        }

        public LocalWebView(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        public LocalWebView(Context context, AttributeSet attrs, int defStyleAttr) {
            super(context, attrs, defStyleAttr);
        }

        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
        public LocalWebView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
            super(context, attrs, defStyleAttr, defStyleRes);
        }

        public LocalWebView(Context context, AttributeSet attrs, int defStyleAttr, boolean privateBrowsing) {
            super(context, attrs, defStyleAttr, privateBrowsing);
        }

        @Override
        public boolean onCheckIsTextEditor() {
            return true;
        }
    }


    private class MyWebViewClient extends WebViewClient {

        @Override
        //show the web page in webview but not in web browser
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }



        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);


            if (customDialog.isShowing() && !dialog.isShowing()){
                AddCard.this.customDialog.dismiss();
                dialog.show();
            }


            if (url.equals("about:blank")){
                if ( dialog != null && dialog.isShowing()){

                    dialog.dismiss();
                    dialog.cancel();

                    Intent resultIntent = new Intent();
                    resultIntent.putExtra("isAdded", true);
                    AddCard.this.setResult(Activity.RESULT_OK, resultIntent);
                    AddCard.this.finish();

                }
            }
        }

    }

    public void addCardToAccount(CardForm cardToken) {


        JSONObject object = new JSONObject();
        try {
            object.put("card_number", cardToken.getCardNumber());
            object.put("card_y", cardToken.getExpirationYear().substring(cardToken.getExpirationYear().length() - 2));
            object.put("card_m", cardToken.getExpirationMonth());
            object.put("card_ccv", cardToken.getCvv());
            object.put("card_holder", "n/a");


        } catch (Exception e) {
            e.printStackTrace();
        }
        final JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Method.POST, URLHelper.ADD_CARD_TO_ACCOUNT_API, object, new Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                AddCard.this.utils.print("SendRequestResponse", response.toString());

                // onBackPressed();

                try {
                    String url = response.getString("url");
                    if (!url.equals("") && response.getString("response_code").equals("20064")){


                        WebView webView = new LocalWebView(context);
                        webView.setWebViewClient(new MyWebViewClient());
                        WebSettings webSettings = webView.getSettings();
                        webSettings.setJavaScriptEnabled(true);

                        webView.clearCache(true);
                        webView.loadUrl(url);
                        webView.requestFocus(View.FOCUS_DOWN);


                        dialog = new Dialog(context , R.style.DialogTheme);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(webView);
                        dialog.setCancelable(false);

                    }
                }catch (JSONException e){
                    e.printStackTrace();
                }

//                Intent resultIntent = new Intent();
//                resultIntent.putExtra("isAdded", true);
//                AddCard.this.setResult(Activity.RESULT_OK, resultIntent);
//                AddCard.this.finish();
            }

        }, new ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AddCard.this.customDialog.dismiss();
                String json = null;
                String Message;
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {

                    try {
                        JSONObject errorObj = new JSONObject(new String(response.data));

                        if (response.statusCode == 400 || response.statusCode == 405 || response.statusCode == 500) {
                            try {
                                AddCard.this.displayMessage(errorObj.getString("message"));
                            } catch (Exception e) {
                                AddCard.this.displayMessage(errorObj.optString("error"));
                                //displayMessage(getString(R.string.something_went_wrong));
                            }

                            AddCard.this.utils.print("MyTest", "" + errorObj);


                        } else if (response.statusCode == 401) {
                            AddCard.this.refreshAccessToken();
                        } else if (response.statusCode == 422) {

                            json = trimMessage(new String(response.data));

                            AddCard.this.utils.print("SendRequestResponse", response.toString());

                            if (json != "" && json != null) {
                                AddCard.this.displayMessage(json);
                            } else {
                                AddCard.this.displayMessage(AddCard.this.getString(R.string.please_try_again));
                            }
                        } else if (response.statusCode == 503) {
                            AddCard.this.displayMessage(AddCard.this.getString(R.string.server_down));
                        } else {
                            AddCard.this.displayMessage(AddCard.this.getString(R.string.please_try_again));
                        }

                    } catch (Exception e) {
                        AddCard.this.displayMessage(AddCard.this.getString(R.string.something_went_wrong));
                    }

                } else {
                    AddCard.this.displayMessage(AddCard.this.getString(R.string.please_try_again));
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("X-Requested-With", "XMLHttpRequest");
                headers.put("Authorization", "" + SharedHelper.getKey(AddCard.this.context, "token_type") + " " + SharedHelper.getKey(AddCard.this.context, "access_token"));
                return headers;
            }
        };

        DocHajj.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    public void onLoadResource(String url){

    }

    private void refreshAccessToken() {


        JSONObject object = new JSONObject();
        try {

            object.put("grant_type", "refresh_token");
            object.put("client_id", URLHelper.client_id);
            object.put("client_secret", URLHelper.client_secret);
            object.put("refresh_token", SharedHelper.getKey(this.context, "refresh_token"));
            object.put("scope", "");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Method.POST, URLHelper.login, object, new Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                AddCard.this.utils.print("SignUpResponse", response.toString());
                SharedHelper.putKey(AddCard.this.context, "access_token", response.optString("access_token"));
                SharedHelper.putKey(AddCard.this.context, "refresh_token", response.optString("refresh_token"));
                SharedHelper.putKey(AddCard.this.context, "token_type", response.optString("token_type"));
                AddCard.this.addCardToAccount(AddCard.this.cardForm);
            }
        }, new ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String json = null;
                String Message;
                NetworkResponse response = error.networkResponse;

                if (response != null && response.data != null) {
                    SharedHelper.putKey(AddCard.this.context, "loggedIn", AddCard.this.getString(R.string.False));
                    AddCard.this.GoToBeginActivity();
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("X-Requested-With", "XMLHttpRequest");
                return headers;
            }
        };

        DocHajj.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    public void displayMessage(String toastString) {
        Snackbar.make(this.getCurrentFocus(), toastString, Snackbar.LENGTH_SHORT)
                .setAction("Action", null).show();
        // Toast.makeText(context, ""+toastString, Toast.LENGTH_SHORT).show();
    }


    public void GoToBeginActivity() {
        Intent mainIntent = new Intent(this.activity, BeginScreen.class);
        mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        this.startActivity(mainIntent);
        this.activity.finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
