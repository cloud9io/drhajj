package io.dochajj.user.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.sinch.verification.CodeInterceptionException;
import com.sinch.verification.Config;
import com.sinch.verification.InitiationResult;
import com.sinch.verification.InvalidInputException;
import com.sinch.verification.ServiceErrorException;
import com.sinch.verification.SinchVerification;
import com.sinch.verification.Verification;
import com.sinch.verification.VerificationListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import io.dochajj.user.Helper.ConnectionHelper;
import io.dochajj.user.Helper.SharedHelper;
import io.dochajj.user.Helper.URLHelper;
import io.dochajj.user.Helper.VolleyMultipartRequest;
import io.dochajj.user.DocHajj;
import io.dochajj.user.Utils.Utilities;
import io.dochajj.user.R;

import static io.dochajj.user.Activities.ActivityMobValidation.MY_FIELD;

/**
 * Created by supzero on 28/01/18.
 */

public class ActivityVerifyOtp extends AppCompatActivity {

    public static final int MILLIS_IN_FUTURE = 120000;
    public static final String LAST_SMS_CALL = "LAST_SMS_CALL";
    ImageView view_back;
    TextView text_pin;
    TextView text_resend;
    Button button_submit;


    Utilities utils = new Utilities();
    Context context;
    View baseview;
    Boolean isInternet;



    private ConnectionHelper mHelper;
    private Config mConfig;
    private VerificationListener mListener;
    private String mPinCode = "";
    private String mPhone   = "";
    private Verification verification;
    private Long mLastSent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_otp);
        context = this;
        baseview = findViewById(android.R.id.content);

        view_back = findViewById(R.id.backArrow);
        text_pin = findViewById(R.id.text_pin);
        text_resend = findViewById(R.id.text_resend);
        button_submit = findViewById(R.id.button_submit);


        initSinch();

        String lastSave = (SharedHelper.getKey(getBaseContext(), "last_sms_call"));
        if (lastSave.length() > 0 ){
            mLastSent = Long.parseLong("" + lastSave);
        }else{
            mLastSent = 0L;
        }
        Long currentTime = System.currentTimeMillis() / 1000;
        Long lastBeforeInSec = (currentTime - mLastSent) * 1000;

        Log.e("TAG", "onCreate: " + lastBeforeInSec + " time " + currentTime + " lasttime" + mLastSent);


        if ( lastBeforeInSec > MILLIS_IN_FUTURE) {
            initSending();
        }else{
            countDownResent(Integer.parseInt("" + lastBeforeInSec));
        }

        view_back.setOnClickListener(new OnClick());
        text_resend.setOnClickListener(new OnClick());
        button_submit.setOnClickListener(new OnClick());



        mHelper = new ConnectionHelper(context);

        isInternet = mHelper.isConnectingToInternet();

    }

    private void initSinch() {
        Intent intent = getIntent();
        mPhone = intent.getStringExtra(MY_FIELD);
        if (mPhone.length() == 11){
            mPhone = "+2" + mPhone;
            Config config = SinchVerification.config().applicationKey("f5d15236-411e-4f96-a2b5-f7a0c5440b55").context(this).build();
            VerificationListener listener = new MyVerificationListener();
            verification = SinchVerification.createSmsVerification(config , mPhone , listener);
        }
    }




    private void checkPin() {
        mPinCode = text_pin.getText().toString();

        verification.verify(mPinCode);
    }


    private void initSending(){
        Long currentTime = System.currentTimeMillis() / 1000;
        verification.initiate();
        mLastSent = currentTime;
        countDownResent(MILLIS_IN_FUTURE);
    }
    private void resendPin() {
        if(text_resend.getText().toString() == getResources().getString(R.string.resend_pin)){
            Toast.makeText(getBaseContext() , "Pin code has been sent to your number " + mPhone , Toast.LENGTH_LONG).show();
            initSending();
        }
    }

    private void countDownResent(int maxsec){
        new CountDownTimer(maxsec, 1000) {

            public void onTick(long millisUntilFinished) {

                long minutes = millisUntilFinished / (60 * 1000);
                long seconds = (millisUntilFinished / 1000) % 60;
                String str = String.format("%02d:%02d", minutes, seconds);

                text_resend.setText(String.format(getString(R.string.resend_after), str));
            }

            public void onFinish() {
                text_resend.setText(R.string.resend_pin);
            }
        }.start();
    }

    private class OnClick implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.backArrow:
                    onBackPressed();
                    break;
                case R.id.button_submit:
                    checkPin();
                    break;
                case R.id.text_resend:
                    resendPin();
                    break;
            }
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    @Override
    protected void onPause() {
        super.onPause();
        SharedHelper.putKey(getBaseContext(), "last_sms_call", "" + mLastSent);


        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        Bundle bundle = new Bundle();
        bundle.putInt(FirebaseAnalytics.Param.VALUE , 1);
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "ua_escape_validation");
        mFirebaseAnalytics.logEvent("ua_escape_validation", bundle);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SharedHelper.putKey(getBaseContext(), "last_sms_call", "" + mLastSent);

        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        Bundle bundle = new Bundle();
        bundle.putInt(FirebaseAnalytics.Param.VALUE , 1);
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "ua_escape_validation");
        mFirebaseAnalytics.logEvent("ua_escape_validation", bundle);
    }

    public class MyVerificationListener implements VerificationListener {

        @Override
        public void onInitiated(InitiationResult initiationResult) {
        }

        @Override
        public void onInitiationFailed(Exception e) {
            if (e instanceof InvalidInputException) {
                utils.displayMessage(baseview , "Incorrect number provided");
            } else if (e instanceof ServiceErrorException) {
                e.printStackTrace();
                utils.displayMessage(baseview , "Service error");
            } else {
                utils.displayMessage(baseview  ,getString(R.string.wrong_ping));
            }
        }

        @Override
        public void onVerified() {
            saveVerfiedNumber();
            utils.displayMessage(baseview , "Verified successfully, Thank you!");
        }

        @Override
        public void onVerificationFailed(Exception e) {

            if (e instanceof CodeInterceptionException) {
                utils.displayMessage(baseview  ,"Intercepting the verification call automatically failed");
//                showCheckValidationCode();
            } else if (e instanceof ServiceErrorException) {
                utils.displayMessage(baseview  , "Validation service error, Please try again later");
//                showCheckValidationCode();
            } else {
                utils.displayMessage(baseview  ,getString(R.string.wrong_ping));
//                showCheckValidationCode();
            }
        }
    }

    private void saveVerfiedNumber() {
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, URLHelper.UpdatePhone, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {

                try {

                    // update phone now
                    SharedHelper.putKey(getBaseContext(), "mobile", mPhone);
                    SharedHelper.putKey(getBaseContext(), "mob_verfied", "1");

                    // refresh at
                    refreshAT();

                    Intent mainIntent = new Intent(ActivityVerifyOtp.this, MainActivity.class);
                    mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(mainIntent);
                    ActivityMobValidation.fa.finish();
                    finish();

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                utils.displayMessage(baseview , getString(R.string.something_went_wrong));

            }
        }) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("mobile", mPhone);
                params.put("id", SharedHelper.getKey(getBaseContext(), "id"));
                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("X-Requested-With", "XMLHttpRequest");
                headers.put("Authorization", "" + SharedHelper.getKey(getBaseContext(), "token_type") + " " + SharedHelper.getKey(getBaseContext(), "access_token"));
                return headers;
            }
        };
        DocHajj.getInstance().addToRequestQueue(volleyMultipartRequest);
    }

    private void refreshAT() {

        if (isInternet) {

            JSONObject object = new JSONObject();
            try {

                object.put("grant_type", "refresh_token");
                object.put("client_id", URLHelper.client_id);
                object.put("client_secret", URLHelper.client_secret);
                object.put("refresh_token", SharedHelper.getKey(context, "refresh_token"));
                object.put("scope", "");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, URLHelper.login, object, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.v("SignUpResponse", response.toString());
                    SharedHelper.putKey(context, "access_token", response.optString("access_token"));
                    SharedHelper.putKey(context, "refresh_token", response.optString("refresh_token"));
                    SharedHelper.putKey(context, "token_type", response.optString("token_type"));
//                        getProfile();


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    try {
                        String json = null;
                        String Message;
                        NetworkResponse response = error.networkResponse;

                        if (response != null && response.data != null) {
                            SharedHelper.putKey(context, "loggedIn", getString(R.string.False));
//                            GoToBeginActivity();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("X-Requested-With", "XMLHttpRequest");
                    return headers;
                }
            };

            DocHajj.getInstance().addToRequestQueue(jsonObjectRequest);

        } else {
            utils.displayMessage(baseview  , getString(R.string.something_went_wrong_net));
        }


    }

}
